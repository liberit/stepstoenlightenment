#!/bin/bash

WPM=150
dot -Tsvg overview.gv -o overview.svg


echo '<ul>' > public/index.html
for file in overview
do
  lualatex $file  || exit 1
  biber $file  || exit 1 
  lualatex $file 
  biber $file 
  lualatex $file 
  lualatex $file  || exit 1

  pandoc -f latex -s ${file}.tex -t markdown_github -o ${file}.md &
  htlatex ${file} "web,next" || exit 1
  cp ./*css public/
  cp ./*html public/

  pandoc -f latex -s ${file}.tex -t plain -o public/${file}.txt 
  espeak-ng -f public/${file}.txt -s "$WPM" -ven-sc -p 75 -w public/out.wav 
  if [ "$1" == "--compress" ]; then
   opusenc --bitrate 6 public/out.wav public/${file}.opus 
  else 
   opusenc public/out.wav public/${file}.opus 
  fi 
  rm public/out.wav 

  wordCount=$(wc -w public/${file}.txt|awk '{print $1}') 

  echo '<li><a href="./'${file}.html'">'$file.html'</a>' \
    "$wordCount words"\
    '</li>' >> public/index.html

  echo '<li><a href="./'${file}.opus'">'$file.opus'</a>' \
    "$(opusinfo public/${file}.opus |grep -i playback.length)"\
    '</li>' >> public/index.html

  cp ./*pdf public/
  echo '<li><a href="./'${file}.pdf'">'$file.pdf'</a>' \
    "$wordCount words"\
    '</li>' >> public/index.html
done 
echo '</ul>' >> public/index.html
echo 'Submit comments, criticism and issues on <a '\
 'href="https://gitlab.com/liberit/stepstoenlightenment/issues">gitlab</a>.'\
  >> public/index.html
