#!/bin/bash
project=stepstoenlightenment
# refresh repo
sudo rm -Rf docker/${project}
git clone  . docker/${project}
# build docker images
docker build -t ${project}-compile -f docker/compile.docker docker
#docker build -t ${project}-emcompile -f docker/emcompile.docker docker
#docker build -t ${project}-quiz -f docker/quiz.docker docker
#docker build -t ${project}-emquiz -f docker/emquiz.docker docker
echo " COMPILING  --------------------------"
sudo docker run \
  --mount type=bind,source="$PWD/docker/${project}",target=/srv/app \
  -ti ${project}-compile || exit 1
#sudo docker run \
#  --mount type=bind,source="$PWD/docker/${project}",target=/srv/${project} \
#  -ti ${project}-emcompile || exit 1
#echo " TESTING  --------------------------"
#
#sudo docker run \
#  --mount type=bind,source="$PWD/docker/${project}",target=/srv/${project} \
#  -ti ${project}-quiz || exit 1
#
#sudo docker run \
#  --mount type=bind,source="$PWD/docker/${project}",target=/srv/${project} \
#  -ti ${project}-emquiz || exit 1
