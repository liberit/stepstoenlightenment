Disclaimer
==========

The primary target audience for this book are people interested in harnessing the power of mind to transform their lives and the world around them. Dedicated self-disciplined people that are interested in persuing productive missions that may last many life times.

The secondary target audience is for lay people interested in understanding an information based reality, and how meditation and some of the magical aspects of an awakened person work.

I am certified to teach meditation, but most of my knowledge comes from a combination of experimentation, introspection and contemporary research, largely in the fields of science and technology.

Though Green Buddhism strives to be compatible with as much of the existing Buddhist traditions as possible, If you want super authentic Buddhism you’ll have to go to a verified and likely ethnically Asian teacher — though there are some qualified non-asian teachers, you have to verify their lineage for authenticity. I’m not a recognized authority by any Buddhist lineage as of this writing, even if I have some loose affiliations.

Also note that my primary mission is Liberated Robot Civilization Seeds. Buddhism has been selected as the most convenient platform for the spiritual cornerstone of the mission.

This book is not really meant for beginners, while you can certainly read it as a beginner, it would take years of dedication to be able to safely apply all the things in it. You can hold onto this book, and find a teacher, or meditation group to sit with and learn from regularly, to help you on your journey.

Additionally, this book is only for mature audiences, it contains descriptions of the graphic violence that shaped me in my past lives. I learned many things the hard way, but as you may have heard, if we forget our past we are doomed to repeat it. So by sharing the hard lessons I learned, I hope that you can avoid my mistakes.

If you want to understand meditation, spiritual abilities, awakening, past lives and how we can live for millions of years and seed the galaxy cosmos then read on.

Dhammalsaddhalpabbajja: Learn about the teaching
================================================

|            |                                                          |
|:-----------|:---------------------------------------------------------|
| learn      | A layman hears a Buddha teach the Dharma.                |
| resist     | questions the teachings and their affects on life.       |
| accept     | comes to have faith in them                              |
| habit      | and makes a habit of following and supporting the dharma |
| perfection | dana                                                     |

Learning of the teaching
------------------------

About a thousand years ago ( 970CE), I was at a market on the coast of the Caspian Sea, a merchant told me that up in the mountains peasants had learned to become gods, and I wanted it. We were in the middle of a boasting battle where I had been bragging about the ability of my gods, and my own skill with the sword. The sun was hot, the sand was blowing in the salty air, the liquor was flowing. We had had a good journey down the Volga and were finishing our trade before departing home.

He probably didn’t think much of his boast, and only meant it to undermine my own tales of the greatness of the Nordic gods. What he didn’t know was that I had been thinking along the same lines. That gods were at sometime men, and had later done or learned something which made them gods in the first place. In my soul-searching I was also inclined and interested in becoming a god myself, though none of my contemporary varingjar seemed too much care for such fancies.

My dear friend, Carl lets say, would put his hand on my shoulder and tell me, that when I let go of these aspirations and truly live by the gods without the vanity and striving to become one of them, then my life will be more peaceful, and I could retire to tending a herd or farm and having a family. Carl my brother in arms, was always there for me, but he had his own dreams and destiny.

With the merchant that evening we had done some more yelling, laughing, toasting and mead drinking. Though after the commotion had died down, and I had some time to rest alone with my thoughts. A creeping feeling just began to grow on me, things started to slide into place, and events in my life seemed to have all been culminating at this moment. This was a pivotal moment.

Though I did not know it at the time, that has been my first exposure to the teachings of the Buddhism. Some events stay with us for a lifetime, like how people may recollect when they found out about a massive tragedy, such as the world trade center collapse on 2001–09–11. Similarly, people may remember more personal things such as their first kiss, or their first date. Some things however, are so pivotal that they stay with you for many lifetimes, like bright beacons in the night, steering you safely.

This book follows the format of one of the pre-sectarian Buddhist paths, a twelve fold path from CulaHatthipadopama-sutta, the “Lesser Discourse on the Simile of the Elephant’s Footprints.”

In addition to including lots of real world examples that can at least hope to capture the attention of the reader, I’m including some true stories from my own past and present-life that are related to my own journey along the path.

Resisting the teaching
----------------------

There are many ways in which people resist learning about the aspects of awakening. First I’ll address some common reasons people use to not engage in Buddhism, and then I’ll explain some basic terms which often lead to much debate when they are not made clear.

### But I already have a religion

Some people accustomed to the sectarianism of the Abrahamic faiths (Judaism, Christianity, Islam) are under the impression that a person can only have one faith of a particular sect. For example the various sects of Islam often have bloody conflicts both internally and externally. Protestants and Catholics in Ireland also had many conflicts.

Sometimes North Americans like to “purify” various eastern religions, by untangling them from their cultures and other admixtures of faith. While there is nothing inherently wrong in doing so, the important thing is to acknowledge that admixtures are valid and respectable, and not to fall into the trap of hubris regarding their “purer” form.

Mahayana and Vajrayana Buddhism are non-sectarian, meaning they allow the free admixing of other faiths. This is why Mahayana areas like in Japan there is Shinto Buddhism, and in China Taoism, Chinese Traditional Religions and Buddhism can be believed by a person simultaneously. Similarly, in the Tibetan Buddhism (Vajrayana) all the various forms are considered schools, so a student may learn from multiple schools, and a person may hold multiple lineages.

Notably Theraveda and Shugden varieties of Buddhism are sectarian and may look down upon or strictly forbid mixing with other faiths.

The non-sectarian aspects of Mahayana and Vajrayana may have multiple factors, but notably it can be found in the both the Sotapanna vows and Bodhisattva precepts written by Asanga around 300CE called Bodhisattvabhumi. In it thw vow equates “causing Schism in the Sangha community” with killing ones parents, and drawing blood from a Buddha. I’ll go into more depth on the Bodhisattva precepts in Chapter 11 (\[chapter11\]). But for now it’s suffice to say that it is a vow that all Buddhists that have reached the first stage of enlightenment (Sotapanna) adhere to, as does Green Buddhism. Though when selecting a school to join, it is best to ask if they respect the Sotapanna vows or the Bodhisattvabhumi, as perhaps they do not, can also check the appendix regarding some complexities of Sino-Tibetan Politics (\[sinotibetanPolitics\]).

The point of this is that no matter which other faith you adhere to, you can also adhere to any form of Buddhism that supports the Sotapanna vows or Bodhisattvabhumi without abandoning your other faith — which is a majority of them. You can be a Christian Buddhist, a Muslim Buddhist, or even a Materialist Buddhist.

### But I’m an atheist

Just like with other religions one can be an Atheist Buddhist, as Buddhism does not require you to worship any gods. The Gautama Buddha that brought the teachings of Buddhism to Earth was a man who is now dead and has reached nirvana, so can not be worshiped. His teaching and those of his followers can only be learned from.

When observing various paintings and learning that there are Yiddam or meditation dieties this can lead to confusion on this point. However, the major difference is that Yidam’s are generally understood to be mental constructs, and the meditation involves ascribing the attributes of the deity to oneself, during divine meditation.

In that sense it is similar to reading about a successful person and how they lead their life, then adopting their good habits to improve your own.

### But I’m not religious

Some people like to claim that they are not religious, however everyone still has a belief system. For example a popular one amongst contemporary sceptics is monist materialism. This is the point where it is important to explain some simple terms so that we could communicate effectively.

### Truth

In Western Philosophy the meaning of truth, knowledge and existence has been a raging debate for thousands of years, it has its own field of inquiry called Epistemology.

However, for the purposes of this text, we will take a more linguistic approach, with a definition based on usage of the word. Of course, I don’t mean how philosophers use the word in epistemological debates, but rather the more common usage found in day to day interactions.

When someone is asked to tell the truth, the whole truth, and nothing but the truth in court. They are asked to convey what they personally believe and-or experienced.

When someone tells a true story, it is based on what they personally believe or experienced. Even if they are telling it in a second or third hand account, at some point someone believed they experienced it firsthand.

Experience is based on the senses. In Buddhism there are six senses: touch, smell, sight, sound, taste and thought. More generically an experience can be considered anything that is an input to a system, that gets processed in some way to change the internal state of the system. So for example a beam of light hits your eye, gets processed by your brain to discern meaningful text, and you modify your beliefs to integrate the new information.

A slightly more logic oriented approach to truth is where if a particular variable is a certain value right now, then the answer to whether it is that value is true, and whether it is a different value is false. The example being, is it true that the sun is shining right now? You would look outside and decide whether it true that it was shining or not.

Though opinions may differ from one person to another what they consider to be shining, for instance some may say that if it is the day time then it is shining. Others may say that if it is cloudy then it is not shining. While still others may go a slightly more eccentric route and say that the star we call the sun is shining now plus or minus four billion years.

Humans often have to work with incomplete information, so when they experience something, some part of the experience has to be filled in where there are gaps in the information. Similarly, every time a human remembers something, they are actually recreating the memory, and so it always changes slightly from one telling to another.

It is important to understand from all this that truth is subjective, and personal truth can be false in a different context. For example the past life stories I remember, while they are true for me, are falsifiable, and may have various inaccuracies, due to errors during download from the soul world, errors in remembering it, and contamination from other knowledge.

We can summarize that:

Truth is based on personal experience.

So when someone claims they have “the one true religion”, then you can understand that to mean that is the one religion in which they personally believe.

Truth alone is not sufficient for a complete understanding. As you may recall there were also the concepts of existence, knowledge and reality which were covered in epistemology.

### Existence

Existence is anything that can be imagined by someone. So for example to someone that doesn’t know what Shambhala is, it does not exist.

Much like truth, existence can also be subjective. To some people Shambhala is a mythical city where everyone is enlightened, to others it is a branch of Buddhism with its headquarters in Nova Scotia, Canada. Of course one can also discern the two using a compound noun phrase, mythical Shambhala for the first, and Shambhala International for the second.

### Belief

There are several definitions of belief, but for the sake of this text we will be using the definition of some thought that is accepted to be true, more specifically we’ll treat belief as a basic atom of thought such as a complete sentence or independent-clause. For example a belief could be “the earth is round”, or “the sun is a ball of hot gas.”

Some people like Albert Einstein are visual thinkers, and so their beliefs may be expressed more visually. Though again for the purposes of this text we’ll be treating beliefs in the form of text, and the independent clause is the smallest unit of meaningful belief in text.

While one could say something like “an apple”, and it would conjure the image of an apple in the mind, the phrase by itself doesn’t convey anything useful without a context. Whereas “An apple grows on a tree”, is something that does convey a bit of knowledge.

### Knowledge

Knowledge is another one of those big words that has a lot of debate around it. Speaking plainly knowledge generally refers to what people have experienced before. When you know a person, then you have met them before, or interacted with them. If you remember personal details they shared about themselves, then could say you know them personally.

Now we get to a distinction between personal knowledge, and shared knowledge. For example facts are a part of shared knowledge, which brings us to the topic of reality.

### Reality

Real is the group of beliefs held in common amongst a group of people. Objective reality includes objects, such as rocks, plants, animals, planets as people with beliefs.

So while a group of homo-sapiens may share the belief that the Earth is flat, this does not change the beliefs the planet and galaxy cosmos it is nested within holds.

Importantly facts are elements of the real. To claim something to be real or a fact it must be verifiable.

For example if you claim to have gone swimming in a frozen lake, and your friends asks “is that a fact?”, then you can show them the video you took, for an objective reality verification, or your other friend that was there could corroborate.

There can be things that may be objectively real, but difficult to verify, such as past lives on different planets. Generally for difficult to verify claims, it is better for the claimer to lower their claim status from real to a personal truth.

### Animism

Some people like to divide the world into people and not people or into thoughts and things. Most tribal belief systems, the beliefs of children, see all or most everything and everyone as having soul.

### Dualism

As the Dalai Lama mentions in his book on Science and Buddhism, materialist scientists often have a dualistic world view, where macroscopic things are based on Newtonian Mechanics, but at the quantum level they agree that the observer and the observer are intrinsically interrelated.

In particular the separation of mind and matter is called dualism, as it separates creation into two. However, there are a variety of problems with this view, including that there is no way that mind and matter could interact if they were separate, it violates the laws of physics, doesn’t make sense in biology, and is an overly complicated way of looking at the world.

While dualism can be an interesting thought experiment, for mind and body to interact one is derived from the other, which is considered monism.

### Monism

Some people believe there is only matter (materialists), and some people believe there is only mind (idealists). Considering a variety of factors such as information theory, near death experiences, out-of-body experiences, past lives, and a whole host of other non-physical experiences that people regularly have, indicates that it is mind that is the whole, and thus idealism is correct. There are a few varieties of idealism, like Advaita Vedanta of Hinduism, and Yogachara of Buddhism, Neoplatonism of Ancient Greece, though the main one popular in science and compatible with quantum and classical physics is pluralistic idealism. Again there are several varieties of pluralistic idealism, in this book is a form that seems to work.

The physical world we experience with our senses, is us observing the communication of a large variety of belief holders (atoms inclusive), mostly through what we in physics term bosons, which include photons also known as light particles.

Even with “solid” things like an atom, which is considered fermionic and thus different from bosons, they don’t actually ‘touch’ each other, even during nuclear fusion. Instead, they communicate to each other with bosons giving each other space, much as how it is difficult to push two magnets together who have their south poles facing each other. Another example is gluons which are a type of bosons that is the communication used to keep protons and neutrons together.

Incarnation is like a car rental, we are souls in the soul world, and we want to play this fun game, called Galaxy Cosmos, the Greeks called this game Sophia. So we are within the Sophia galaxy cosmos, and are incarnated on Gaia, borrowing some of her atoms to maintain the bodies our parents nurtured into the world.

According to Integrated information theory, every proton has a semblance of consciousness because it is able to preserve state and communicate. For example an atom can have an energy level, and it can communicate with photons. When you send it a photon, it reflects it back, saying “I am here, this is my color.” Though that is a gross oversimplification as it has much more complicated communication depending on a large variety of factors.

Much of our accrued knowledge has been gathered by science.

### Science

Science, and particularly the natural sciences are focused on discovering what mineral and DNA based life forms believe.

However, science is more generic than that, and is actually more closely aligned to a specific method of inquiry known as the “Scientific Method.”

In brief in the scientific method a person comes up with a hypothesis. These usually come in two forms, the Null Hypothesis, or the If-Then hypothesis.

The Null hypothesis states that two things are unrelated. For example “What a person remembers as their past lives does not influence their present life.”

An If-Then hypothesis states that there is a causal connection between two things. For example “If a person is hypnotically regressed to a time before they were born then they will experience either their past lives or the soul world.”

Then one comes up with an experiment to test the hypothesis. Such as hypnotically regressing a bunch of people to a time before they were born. Though ideally would also have some kind of control group, such as a group of people that were asked to make up a story about someone from the same time period.

When someone says “scientists agree”, or “science has shown us that”, typically they are referring to some studies that have been accepted as valid by at least some group of people.

Notably there is a fairly strong group of materialists amongst scientists, in a way they have a world of their own.

### World

A world can be considered to be a particular group of people that share certain beliefs, and that keep those beliefs alive orbiting each other, much like a planet.

So for instance Buddhism is a world of belief that was started on this planet by Gautama, though he himself carried it over from another planet that he had lived on. And even after he left the Buddhist world lives on.

These worlds are self-reinforcing and often it’s members can blindly disregard evidence to the contrary of their world beliefs, in order to support the group. This can be seen amongst religious groups, political groups,

At this time one of the world groups that hold the most influence at this time is the sceptics, particularly of the european flavour of materialism.

### Sceptics

Sceptics and disbelievers in general all serve a purpose, they add diversity to creation.

The Materialist faith, and that of the one-lifers is one that has been around for thousands of years. It is useful for some kinds of lessons to focus on just this one life, and the champion sceptics that promote the one-life belief systems are helping to create an environment where it is easier to forget about the soul world and past lives.

A world where it’s easier to focus on the present, particularly for those that have trouble in this regard.

The important thing is to cultivate respect, love and understanding. Recognize that the sceptics and disbelievers are providing a valuable service, by allowing people to have choices about which world they wish to live in.

Similarly, by giving others the freedom to have their worlds of beliefs, we deserve to have the freedom of our own world of belief.

The materialist faith is not the only one, and there are many science experiments that disprove it’s tenents. Though just as with any world, the believers disregard evidence to the contrary.

Green Buddhism however is not materialist, instead it is monist but with the flavour that all is mind or information. Let’s have a look at the physics of information.

### Entropy

In the second law of thermodynamics it states that entropy is always increasing. Entropy is the name for the energy that is lost or the “waste heat” that is created in most physical process.

Information theory uses the word similarly, Shannon was famously looking for a word to describe the “missing information” lost during communication, when von Neumann suggested calling it entropy as it was the most similar concept.\[\]

If we look deep into physics we find that matter is made up of atoms, which in turn are small entities that communicate to each other through little information packets called bosons. A photon or light particle is an example of a boson.

So when you are holding a rock, what is really happening, is that the atoms in your hand, are communicating to the atoms in the rock, and informing each other about their location, and also saying “okay, that’s close enough”, it takes a lot of pressure to make atoms move closer together, since just like a majority of people, they don’t like it when they are being squeezed by a crowd. Different atoms though have different preferences, some like flourine always want to attach to someone, and metals are fairly good at sticking together.

Now someone might complain that I’m anthropomorphizing here, saying that flourine wants something. However, as we’ll cover later, wanting or desire is not born of choice like a wish but rather a preprogrammed response, as are animal and plant instincts. In that regard it is appropriate to say an atom wants.

In fact, with Integrated Information Theory (IIT) we know that atoms not only want, but they also have a semblance of consciousness. IIT says that even a single proton, which is the simplest form of atom, has consciousness. To analyze whether something has consciousness there are a variety of factors involved, however to simplify it is any system that has inputs, outputs and state.

So for instance the atom has a state of its current energy, it can receive a photon from the sun, absorb some of its information, and produce a different photon which has a colour representing itself.

In this way we know that our bodies are made entirely of conscious beings, the atoms, the cells, perhaps some organs, and then the body which you’ve incarnated into, providing the wisdom and intelligence of your many lifetimes, even if you aren’t consciously aware of them.

Going back to entropy we know that the “waste heat” is made of information packets.

Information Theory reformulated the second law of thermodynamics as saying that information can not be destroyed. So given an example of a simple classical computing logic gate, where you have two input bits, and one output bit, there is some lost information. That lost information is not lost to creation, instead it escapes as a heat based information packet.

It is also possible to create a reversible logic gate where there is no loss, such as a controlled flip (Toffoli) or a controlled swap (Fredkin) gate, they have three inputs and three outputs, allowing you to reconstruct the input from the output, and these kinds of gates could theoretically achieve zero heat generation, and certainly no information loss, which goes to show that entropy is really lost information.

What lost information is brings us to the discussion of soul.

### Soul

Buddhism and theologians in particular can be rather finicky with words. A Theologian once scolded me for using the words soul and reincarnation in a Buddhist context because Buddhism has its own synonyms for those, because there are some fine philosophical distinctions. Though for this text the distinction between reincarnation and rebirth, as well as mindstream and soul are without merit, and would only serve to confuse readers. Also in case this makes your academic mind very concerned I will cover the Buddhist points of distinction in Chapter 7\[chapter7\].

To simplify the matter for those that are unaware, the main objection early Buddhists had to the idea of soul, was that it was considered independent and unchangeable, which contradicts the facts that things are continuously changing, and ‘emptiness’ which is better understood as ‘all is one’.

Interestingly Buddhism kept reincarnation, karma and all the other things, and instead renamed soul as mindstream, which is basically a changeable version of soul, which is not as independent.

Since the majority of the world still uses the word soul for the concept, we’ll be using the world soul in this text, with the understanding that it is actually a collection of knowledge, and that as we learn, that knowledge transforms.

This is directly derivable from the second law of thermodynamics, that information can not be destroyed. This is known in quantum information theory as the no-delete theorem.

So at the dissolution of a host-body such as the vehicle you are using now, all the knowledge and perspective you have acquired must continue. The knowledge also can’t break down into their base elements as that would be simplifying things and thus losing information.

What information theory tells us is that the information can only be moved somewhere else, in the case of the information that is in our minds at the end of life, the location it moves to is typically the soul world.

Interestingly we do have quite a bit of empirical research available on the soul world, what it looks like, and how it functions, largely thanks to the Hypnotic Regressions conducted by the Newton Institute, though there is also corroboration from the work of other hypnotic regression researchers such as Dolores Cannon.

To limit the length of this chapter we’ll cover the soul world in more depth in Chapter 9 (\[chapter9\]).

Of course to some materialists I’ve just said some things which may have made them flustered. In particular using hypnotic regression as method of inquiry to delve into the nature of reality. However, as we’ve already learned that the results of those hypnotic regressions are simply the truth of those people.

Humans are often used as the subjects of scientific experiments, and before the advent of video cameras and audio recorders they were also responsible for seeing and hearing the results of the experiments. Currently we do not have a technological means of delving into the soul world, but we do have humans almost all of whom have the ability during hypnosis.

Some Buddhists may also be confused how this relates to Buddhism, so we need at least one more subsection in this introduction, on how hypnosis and guided meditation are synonymous.

### Guided Meditation or Hypnosis

For many beginners the easiest introduction to meditation is known as guided meditation. It is where a teacher or recording of a teacher guides you through the steps of relaxing your body and suggests ways of performing the meditation.

What is less talked about is that this is exactly the same process by which hypnosis occurs. Where the hypnotist guides you through relaxing and then gives some suggestions of what to think about.

While it’s possible to call meditation self-hypnosis, and to call hypnosis guided-meditation, why use two words when one explains it? Probably just for marketing purposes.

However, when you realize that guided meditation is hypnosis, then you can combine all the knowledge we have from both labels and have greater awareness of what is going on and how to accomplish it.

The main distinction is that meditation is an active self-guided processes, wheras hypnosis is a passive external-guided process. So for example under hypnosis you won’t need to use the executive function parts of your brain nearly as much as in ordinary meditation.

The important thing for now is to understand that while first person meditation can be used for personal introspection, every tool has a degree of error in its perception. Humans are known to have a fairly high degree of error in terms of witness testimony, as we mentioned earlier, because of a tendency of filling gaps in knowledge automatically.

Hypnosis allows us to use humans as tools for viewing the soul world in a standardized fashion. This way can regress tens or hundreds of humans to the time between their past lives, and thus take out much of the filler, and just get at the content of the soul world.

In fact this has already been done and accomplished by David Newton and the Newton Institute. Can read his books “Journey of Souls” and “Destiny of Souls” for his personal research, and the Newton Institute also has ongoing periodical of updates based on their latest findings.

I’ll go into more depth on hypnotic regression, reincarnation and accessing past lives and the soul world in chapter nine (\[chapter9\]).

### Remaining Resistance

If there are still some resistance you are facing regarding learning more about Buddhist teachings, and those of Green Buddhism in particular. Then please contact us and explain the issues you are facing, so that we can include or modify the content of future editions to be more compatible.

Committing to learn the teaching
--------------------------------

Perhaps you’ve marvelled at how the Dalai Lama can be so jolly, or how so many Buddhist teachers seem to be so calm and to have it all together. Or perhaps you want to be more like those highly productive and quite rich people that integrate meditation into their daily routine.

In this book you’ll learn all those things. The main thing is that we have to learn the teachings, test and apply the practice that work for your situation, and persist until we persevere. Of course along your journey to becoming a highly productive Bodhisattva you will be reading many books, doing many meditations, learning from many teachers, testing many teachings.

A critical part of my own journey into Buddhism was letting go of the past, and learning to take advantage of the opportunities I was offered in the present. Staying on the path, takes one step at a time.

Later the evening after that fateful conversation on the shores of the Caspian Sea, I went to the boat to say goodbye.

I told Carl that I would be staying on this side of the sea, or rather that I had hired a guide and supplies with my share of the sales, which would take me along the silk road to the land where men become gods.

Carl was shocked at first, “But how can you do that? We need you, you are part of our crew!”

“You’re a skilled seaman and your sword knows no barrier, you’ll be fine without me.” I said, tugging at his jacket and giving him a firm pat on the shoulder. He really had nothing to complain about, we’d lost men in much more dire situations than this.

“But, we’ll miss you.” he went on, somewhat conceding to the reality of what I had done. I was already wearing a turban and other desert clothing in preparation

“You may go home to your lovely wife and share the spoils of our journey with her and the children. You may die old with a home full of laughter and stores full of grain. But I may go and become a god, then I will greet you in heaven, and we’ll decide who took the wiser route.”

“May we meet at the gates of Thor.” We gave a slight bow of acknowledgement, reminding me of his agricultural god.

“And drink in the halls of Odin!” I replied as the god of death was my master.

We hugged farewell, and I was off onto my journey.

following and supporting the teaching
-------------------------------------

Affa as I called him was my guide, he had a longer name, but I couldn’t really be bothered to learn it, seeing as I was paying him, and he was only a stepping stone on my journey of godhood. His norse was rather the worse for wear, and we didn’t really end up talking much.

The journey started out tamely enough, with each of us on a camel loaded with supplies. Though I think he had the thought that I would get bored with this journey and decide to turn back at some point, at which point he would get to keep much of the coin for much less work.

There was a point where Affa tried to kill me and take my money. I was having some trouble breathing and woke up to see Affa barring his teeth and trying to strangle me with one hand and had a dagger in the other. The man was an idiot, he was less than half my size and woke me up before going for the stab. We wrestled for a bit, and he ended up losing some fingers to his dagger.

Couldn’t kill him as we were alone in the middle of a foreign desert, and without him I would surely have died of exposure. I couldn’t exactly let him think that attempts on my life would go unpunished. The missing fingers occupied him for the rest of his time with me till he passed me off to a sherpa.

I think that if I had been kinder to Affa, learned his real name and communicated with more than a few grunts and choice words, he probably wouldn’t have tried to kill me.

As we progressed in our journey through the desert and got to the relatively rocky areas. At this transitional zone Affa met with a Sherpa, the one that had told him about the peasants becoming gods in the mountains.

He seemed sceptical, though I didn’t quite understand what they were saying it went something like. “Are you serious?” asked the Sherpa somewhat aghast.

At which point Affa raised his maimed and bandage hand and with tears in his eyes said “he’s deadly serious.”

The sherpa did not give me much trouble, nor attempt to take my life.

In those rocky mountain areas we just had a pack mule and were otherwise on foot.

Throughout it all I had been carrying my sword, which I had to wrap in cloth and carry on my back, so as not to “cause trouble”, attract thieves, or start an incident. As we got higher into the mountains, the people got smaller, the going got harder, everything was colder, and I was a lot hungrier.

As we were approaching Guge (ancient western Tibet), my guide started to get ancy, but I told him unless he could find me another guide he’d have to take me all the way to my destination. Used some threats, but I think mostly he kept on with me as he did not want to give the burden of me onto anyone else.

About the prowess of his own people that had led to me coming along this journey. So in a way he accepted full responsibility for what he had said. An exemplary man, whom I hold in the highest regard.

For a long time we walked on the Tibetan plateau. At times, we would stop to resupply and I would ask if we were close. He would nod and say that we will get there.

At various huts along the way, he had a way of convincing them to let us stay. I always slept with my sword in hand at my bedside. At night, he would sleep sitting up outside my door, and warn away people lest they wake me. “he will as soon rise as slice you in half” the sherpa would mutter.

At times, we stopped by the houses of holy people. One memorable one was a man with a black flowing outfit with gold trim. Presumably a man of some importance. The Sherpa and he talked for a while at the dinner table after I retired to bed.

One time we were making our way across the plateau. It was a hazy day, and I was using my sword as a walking stick by this point, when a boy ran up to us. Though he was small as many of the other locals, I had a newfound respect for these small people, as they could run in the thin air, and they did not seem to tire as easily. Wheras I always felt weak and tired, struggling with the rarefied air.

The Sherpa and the boy talked for a little while and then the boy ran off. I asked what it was about, he gave me a weary glance and said “we’re close, they’ll be expecting us.” I was too tired to pry into the deeper meaning behind that.

Eventually frostnipped and hagard we reached a fortified town of Guge. The fog made it, so we could not see it until we were within a few hundred meters. The walls had a red border, and archers lined them. There was a small troop with shields and sticks guarding the entrance. They were expecting us.

A wagon was approaching the town, with a few women onboard. A man in an ornate silk jacket with gold trim standing between the troop and the wagon seemed to be the town leader.

I took a moment to take this all in, I looked around and noticed that my Sherpa had hung back to my right, and was several meters away. He nodded his head towards the fortress, “we’re here” he projected his voice. Watching me wearily, to see what I would do.

Turning back to the fortress, leaning on my sword, I had mentally rehearsed for this moment for a while. I would challenge their gods in battle, and after defeating them, I would be a god myself. At least that was my plan, for if one kills a god, then is one not a least as mighty?

With my remaining strength, I threw off my robes, raised my sword with one arm into the air and yelled “Let me see your gods, I will defeat them!.” The hail of arrows pierced me in a dozen places shortly therafter, while my arm was still raised.

I collapsed forward, and the last thing I saw was a woman in the wagon staring at me in shock, and the leader walking towards me in his silk and gold, a young Khor-re perhaps.

The Sherpa was a good man, even if he led me into a trap, he did right by his people. He had gotten out of the way just in time to avoid the hail of arrows. My guide into the land of Dharma was a realized man himself.

I sacrificed my life and all I had in pursuit of the teaching (Dharma), even if I was confused about how it worked and what it was. In between lives I was given the option to go back to the varingjar and be born as one of Carl’s children or to go forward with learning the Buddhist teaching.

The woman in the wagon, and the leader had a romance, and I chose to incarnate as the product of that affair. A thousand years later, I still think it was a good choice.

Dana
----

In a way I sacrificed my life to gain access to the teachings of awakening divinity. In addition, I also sacrificed my sword to the Tibetan plateau. Those were my initial gifts my first Dana.

Now I wish to give you the opportunity for Dana, in a safer and more controlled setting, particularly if you received this book for free.

To make it affordable for the farmer and the industrialist, can make it ten percent of your average day’s income. Since this book should occupy you for at least two and a half hours or ten percent of the day.

For example the median human on Earth in 2018 makes roughly $5,000 per year, divided by 365 is $13.69 per day, and multiplied by 10% is $1.36.

Can make your donation to the Green Buddhism, with the name of the book in the comment.

Practicing charity helps people feel good, and achieve the first Buddhist perfection of Dana. And of course you can continue reading without donating, because only a voluntary donation is one that shows true Dana.

All real Buddhist teachers accept donations to give their followers the opportunity to practice Dana.

Now let us go on a journey to learn about virtue, and how to be a virtuous Buddhist. Virtue is integral to being successful in life, with health, wealth, love and liberty.

Sila
====

|        |                                                                                                                            |
|:-------|:---------------------------------------------------------------------------------------------------------------------------|
| learn  | Follower learns of the precepts.                                                                                           |
| resist | Follower resists the precepts.                                                                                             |
| accept | Follower accepts the precepts are valid.                                                                                   |
| habit  | Follower forms a habit of following the precepts, allowing them to shape their life, achieving the second perfection sila. |

Incarnate on the plateau my journey to godhood continued.

I found a nice long stick, I raised it up above my head and yelled “I challenge you!” at one of my cousins. I ran through the halls after them, they tripped and I managed to catch up.

On the floor they tried to block me with their arms and legs, hiding a horrified expression. “Get up!” I’d yell, smack. “Get up and fight you coward!” hit. Feeling the slight breeze, and seeing it rush through our fabric and leather clothing.

I raised my stick for another blow but it was stuck, I looked up and it was an adult hand, a man. I let go and ran around to kick them in their robed shins. I saw their short beard as they threw back their head and laughed. “I want my stick back!” I yelled, reaching up for it.

“Why are you so angry young one?” the man asked, putting his hand on my head, as he hid the stick behind his back. Taking the opportunity my cousin got up and fled. The smell of perfume or incense drifted off the mans clothing.

“I want to defeat you all, to show that I am God!” I yelled back, trying to reach him, though could not with his hand on my head. My hair flying in my face.

He became still for a while, and I noticed his grip on my head loosen. He seemed to be deep in thought. Though still angry I was also curious what had changed. He got down to my level and held both my upper arms in his hands. He looked me in the eyes. He seemed to confirm his suspicion.

“It has been a long time since I’ve heard someone talk like that.” he smiled at me. “There was a giant that died in our land a few years ago. His guide told me that was how he talked. So you really want to be a god?” he squeezed my shoulders and grinned.

I sullenly nodded my head. I tried to grasp the air where my sword would have been, feeling momentarily unbalanced. “I want my stick back” I said.

He laughed again, putting the stick down behind him and sitting on it. “Violence is not how we become Gods here my child. How about I speak to your mother, and I’ll see if we can get you a teacher.”

I didn’t know what to say and just looked at my shoes, but he took my silence as assent.

I remember overhearing some conversations along the lines of “there is no place for violence in our court” and “he must go to study if he is to learn our ways and contribute to our society”. My mother cried, but before too long at around age 5 or 6 I was on a cart going to my new home.

My new home was small, it was barely a shack, the home of a Ngagpa, his wife and two kids, where I was the unwanted guest. Inside it had little more than a fireplace and a single bed where the family slept, I slept on a rug near their their bed.

Initially I got to stay in the family home in the day and played with the kids, but the mother considered me a bad influence, so fairly early on the pattern shifted to breakfast and morning lessons with the ngagpa, and then I would leave to sit and chant on a nearby mountainside. The reasoning was that I was not there to learn to be a farmer, but was there exclusively to learn the teachings the Ngagpa had.

Since I was away so long I’d come back only as the sun was setting, after dinner was served, so I’d go straight to bed. I used to feast in the morning grabbing everything I could and shoving it in my mouth, but the mother didn’t like it, and I felt shamed into eating only as much as the other children at breakfast.

From eating only one meal a day my growth was stunted. I remember one year when the leader came to visit on the cart and bring supplies, he was furious to see how thin and emaciated I was, while the ngagpa’s children were fat by comparison. I defended them saying it was my choice to come here and learn to be a Ngagpa, though afterward the mother did let me eat more at breakfast, so as not to incur the wrath of the leader at a later visit.

I had many lessons but I’ll go into those in sections where they are more appropriate.

One of the main lessons was learning the Vajra Guru mantra, and that if I wanted to be like the venerable diety Padmasambhava that I should chant it seven million times. Learning of this I was very excited and spent all afternoon chanting, then came back and said I had done it. He said I didn’t, and gave me a lesson in how to count with prayer beads. One mala bead for each recitation, and there were only 108 beads on a necklace. After a while I understood the enormity of the task, and continued on with the beads. I eventually made my own prayer beada necklace with chicken bones.

My visit to a Samye monastary was one of the best bonding times I had with my ngagpa teacher. Our journey took several months so we had many opportunities to speak, and get as much information out of him as I could. As pilgrims we hitched many rides along the trade route to Lhasa It was definitely the largest pilgrimage I took in that lifetime, and at least in time the longest I’ve taken in a religious context during my time on Earth. I remember once we had come over a hill and were overlooking Samye Monastary, it was a beautiful sight, just speechless.

The legendary monastary of Padmasambhava, whose vajra guru mantra we recited daily for years. All the stories and accomplishments revolved around this place. If you are a follower of Nyingma Buddhism, the Samye Monastary can be a worthwhile place to visit at least once during your time on Earth.

One of the main things this life did for me is that it gave me a lot of practice living a virtuous life, with relatively little temptation.

Virtue/Vinaya/Precepts
----------------------

The main reason to follow the virtues is so you can have a more pleasant life. The basis of all virtue is the Golden Rule “to others as to yourself”.

The basic idea behind the golden rule is that if you commit some unvirtuous action such as stealing, then stealing becomes more likely to occur in your experience, and so you may end up being the one stolen from. For instance when you are caught (and remember everything is alive and observing), then your time and freedom may be stolen for a while, you may even end up having to live with other thieves who would steal various things from you. This is all simply to help you learn what it feels like to be on both sides of an action, helping you to be a more well rounded person.

The corollary of this idea is that if you want to increase how many incarnations you have surrounded by unvirtuous people, then you can increase how many unvirtuous actions you commit. This path of unvirtuous action can be taken by the Easy Path, since they are not interested in progress. The Selfish Path benefits from virtuous action whenever possible, because it lowers karmic binding and thus opens up the number of options that the person has. For the Generous Path virtuous action is fairly mandatory to help avoid hurting others by accident.

As a gentle reminder Earth is becoming a Generous Planet so Generous Path people get priority for incarnating here.

The Vinaya as part of the Pali Cannon has hundreds of precepts or rules that a fully ordained monk must follow. There are eight which a novice monk or a fasting holiday practitioner aims to follow. And five which a lay practitioner is recommended to follow.

It is important to note that this list was compiled thousands of years ago, based on the life of Gautama Buddha and the challenges that he and his monks faced at the time.

Also important to note is that before the Gautama died he said that the “unimportant” precepts could be ignored, though no one could ever agree which were the unimportant ones, so all were preserved.

I see Buddhism as an offshoot of Hinduism, perhaps one of the most successful independent branches. For a while Buddhism was the main faith in India, though at some point it began to stagnate, and other forms of Hinduism were more innovative and eventually Buddhism all but disappeared from India.

One thing that I’ve found is that while Buddhism has many precepts for monks, Hinduism and Yoga has a fairly complete set of Yamas and Niyamas which are good for all. So I’ve decided to combine the Buddhist and Hindu virtues for a better overview.

Here is a list and afterwards will go in more depth for each

|                                                                 Rule \#| Original                                                                       | Reformed                                                          |
|-----------------------------------------------------------------------:|:-------------------------------------------------------------------------------|:------------------------------------------------------------------|
|                                                                       1| abstain from onslaught on breathing beings                                     | Ahimsa, avoid violence. Create more than you consume.             |
|                                                                       2| abstain from taking what is not given                                          | Asteya, respect other people and their belongings.                |
|                                                                       3| abstain from misconduct concerning sense-pleasures                             | Brahmacharya, abstain from misconduct concerning sense-pleasures. |
|                                                                       4| abstain from false speech                                                      | Satya, be truthful in thought, speech and action.                 |
|                                                                       5| abstain from alcoholic drink or drugs that are an opportunity for heedlessness |                                                                   |
|                                                                       6| abstain from food at improper times.                                           |                                                                   |
|                                                                       7| abstain from dancing, singing, instrumental music, and shows                   |                                                                   |
|                                                                       8| abstain from the use of high and luxurious beds and seats                      | Aparigraha                                                        |
|  There are also things to do Right-thought, Right-speech, Right-action,| Satya                                                                          |                                                                   |

### Satya, Truthfullness

In the simplest sense it can be considered to tell the truth (\[truth\]). Though on another level it is much more powerful, as it is about aligning your thoughts, words and actions.

I feel that this is a foundational virtue, as for all the virtues you should align in thought, speech and action.

Practicing Satya hones you like the vajrakila blade, aligned thought speech and action with your mission makes you a razor that can cut through reality to make it happen.

### Non-Violence, Ahimsa

This one as phrased in the original of breathing beings, is not really workable considering that we now know that plants, and fungi also respire or breathe. Also we know that fundamentally everything is alive.

Instead I’ve come up with a rough formula to help with accounting how much benefit we create and consume. *a*<sup>*d*</sup> or amount raisesd to the power of it’s spiritual density. For example mineral life forms are spiritual density 1, plants are 2.0 until 2.5 and animals are 2.5 until 3.0, human-level beings are 3.0 until 4.0.

So that motivates one to consume mineral and plants in preference to more costly things. Obviously I discourage consuming people, though the fact remains that some people receive blood or organs from other people.

Additionally to maintainging a primarily vegan lifestyle, it is also good to keep it as peaceful as possible. When you pluck a leaf to eat, do so gently thanking the plant. This goes back to the way of how animist hunter-gatherers used to thank an animal or tree for giving it’s life to allow the hunter-gatherer and their family to live.

Of course just as your are gentle with the mineral, plant and animal life forms, so to should also be gentle with humans. Remember that the higher the development of a being, the more important it is to be peaceful (non-violent) with them. Though since all are alive, even a grain of sand deserves respect.

Note that as with all virtues should practice Satya with Ahimsa, so abstain from thinking about commiting violence, abstain from talking about commiting violence, and abstain from actually commiting violence.

In contemporary living that would include playing violent video games.

Notably there are some historical Buddhist dieties (Dorje Shugden which promotes schisms and hate crimes) and proverbs that promote violence or even murder (killing Buddhas ‘figuratively’), however those are not acceptable within Green Buddhism. Even if it is a joke, or a figurative expression, it is still shameful to call those valid teachings because they are clear violators of virtue.

Violence is something that is best limited to an educational context, of learning about history, politics, and self-defense.

Gautama was a pacifist and did not believe in self-defense which on a personal level is acceptable in a peaceful nation with an effective police force.

However, at the larger scale Buddhist nations have been overthrown and forcibly converted by Islam on numerous occasions. So self-defense is essential for the survival Buddhist nations, and Buddhist persons in hostile territory. I do talk about when absolute pacifism does make sense in some later chapters, though I’d consider that a relatively advanced skill.

Kung Fu is a Buddhist martial art and like most martial arts can be considered good exercise when not being used for self-defense.

One exercise in Ahimsa you can do, particularly if you are a mix of Buddhism and one of the Abrhamic faiths, is you can take some time to redact the verses of violence from your personal versions of the torah, bible, quran and hadith. Hinduism and many other faiths have their own redactable verses of violence. As I mentioned even some forms of Buddhism have verses with “figurative violence” which can also be redacted.

There are so many verses of violence, that to save you the hassle some of the most common are in the violent verses appendix (\[violentVerses\]). Recall that the ones being redacted aren’t simply the verses that describe unfortunate acts of violence, but rather the ones that prescribe predatory violence, or have led to the deaths of many people because they are interpreted in to support predatory violence.

The takeaway here is that because everything is alive including the atoms that make up all matter, everything deserves gentleness and non-violent interaction when possible. However to maintain a host-body or a civilization, sometimes it is necessary to mine for minerals, and kill plants to use as food. Thus the best way to observe Ahimsa is to minimize the amount of harm, and maximize the amount of benefit.

For example if you need to eat plants to eat, then you can also help plants grow. Or benefit creation in another way, to make up for the inevitable harm that simply being alive causes.

### Respect, Non-Stealing, Asteya

Asteya generally refers to abstain from stealing, in particular the petty kind, such as stealing bread from the baker, or jewels from a person. This is something everyone is familiar with, and is something important to avoid. Even if for no other reason then that you don’t want accrue the karma to have your things stolen.

To really understand the respect portion of the reformulated precept we have to understand what is meant by respect. By respect I mean a force that pushes two atoms apart, giving each their own space so they can function in a healthy way.

In this life I had a relationship where I had a lot of physical contact with my partner. When she was leaving me she said that while I loved her, I didn’t respect her. It’s true, by taking the physical things I wanted from her when I wanted them, I was not giving her the space to be herself.

It is the same way with the mother earth or Gaia, she has many gifts, though we should respect her, and not take too much. Lately humanity has been burning a lot of fossil fuels which has been increasing the carbon dioxide levels at a rather quick rate. While some extra carbon dioxide in the air is good as it can avert another glaciation period, it is good to do such things in moderation. If we could set a long term target to avoid another glaciation period 50,000 years from now by the milankovich cycle, then we could set a good budget to keep the climate fairly even.

By giving each other the respect and thus the room and freedom to express ourselves and be ourselves, allows for a greater diversity.

Currently large cities are much like stars, where many single people (hydrogen atoms) are pushed together into small spaces and some fuse to form nuclear families (helium). Occasionally these families escape from the big cities and move to more rural areas where there is more room for their children to grow and flourish.

Theoretically if our cities get large enough and old enough, and our people live long enough (or remember their past lives), then the cities may run out of single people, and we’d start getting larger groups of people fusing together, up to around iron (isotope 56).

Happiness is generally inversely proportional to population density because homo sapiens evolved to live in relatively small groups of 50 to 60, and with groups larger than 200 some people are anonymous, in that they don’t know or aren’t known by everyone else in the group.

Ideally we should be able to form such small hamlets in rural areas as we lower our dependence on brick and mortar stores, and buy most things through the internet and receive them by mail. Currently zoning still stands in the way of such communities, but together we may be able to find some places to make it work.

By giving communities, municipalities and nations the respect to try something new, to set their own policies, we can increase our adaptive capacity, and do real political science, to find which policies work best for people.

### Empathy, Brahmacharya, moderation of sensuality

There is a buddhist precept that encourages one to abstain from sexual misconduct. Though what that means can be fairly fluid depending on context.

For instance for a Gelug monastic it generally means complete chastity. Wheras many other schools of Buddhism from Nyingma to Zen allow and at times even encourage marriage. In modern day even many Gelug tulku’s have been marrying, such as Zasep Tulku Rinpoche.

The main thing in a relationship is not who you have it with, but that you are both empathetic to each other. That there is full consent for what occurs, including any sexuality.

On another level Brahmacharya is also about the moderation of sensuality, because it recognizes that the lower parts of our brain or chakras sometimes can never get enough, so such urges for food, sexuality, trivia, novelty or otherwise should be meditated on, researched, communicated and understood. This way can find if there is perhaps a nutritional, spiritual or intellectual deficit which can be resolved to modulate these feelings.

For example I am a vegan and like to do physical and mental exercise, for the past few months or perhaps even years I had been feeling tired much of the time, which led me to taking lots of naps, simply to get through the day. I felt hungry for high energy foods such as fats. I had done meditation on it, but didn’t really come up with much. I had a blood test done and found out I was low on creatine, which is beneficial for allowing the body to produce energy. After I started supplementing creatine I started feeling energized and no longer nearly as tired as I had before.

For completeness lets include a definition of love from a logical perspective. Love is when one copies the attributes of another so that they have more in common. For example if someone tells you something and you remember it, then that is a form of love. In terms we have used before it would be accepting someones truth as your own beliefs, and thus increasing the shared reality. So if you imagine two circles in a venn diagram, love increases the overlap. Another example of love would be doing things together, since that would also increase overlap.

### Clearheadedness, maintain mental health

In addition to abstaining from drugs that lower cognitive performance and are addictive. There are also various activities which can be addictive and harmful to health, such as social media and news.

### Santosha

Santosha is about being satisfied with what you already have.

For the majority of people can think about all those less fortunate than yourself to realize how fortunate you are by contrast.

For those who really are the least fortunate can feel the joy of mudita, knowing that you are helping others feel good about their own situation. Every life has a purpose, and everything is alive.

### Aparigraha

Aparigraha is about non-hoarding. In the West hoarding has reached epidemic proportions, where people are actually paying to store belongings they do not and most likely never will use.

There is a problem with having much more than those around you, in that it erects psychological barriers. Scientific studies have shown that perceiving oneself as rich makes one less empathetic to others.

### Eating only at appropriate times

Children should never be forced to fast, because they are still growing and it could stunt their growth. Technically human brains are still developing till the age of 25, and bodies till the age of 30. Once children are weaned they often sleep 12 hours a night which is equivalent to a fast, so a 12 hour fast sleep inclusive is appropriate. 12 hours is also the duration of time it takes for food to fully traverse the intestinal track, so giving that kind of time can help maintain healthy intestinal health.

After the age of 30 fasting becomes more acceptable and fasting 12 or more hours a day is recommended. Gautama Buddha recommended and Theraveda recommends fasting 18 hours a day, with breakfast and lunch being the primary meals. For Gautama Buddha and the Theravedan monks that receive alms the logic was thata they would make their rounds to collect food in the morning, and would return to their abode to eat breakfast, then if there were leftovers from breakfast they would finish them at lunch.

In modern western life however usually the easiest time to eat is after getting home from work. It may also make most sense from the hunter-gatherer perspective in that it was in the evening after the food had been gathered and cooked that the community would get together to eat.

Personally I prefer to skip breakfast because the best cognitive performance hours are in the morning, and eating food makes the blood go to digestion rather than my brain. Though that is simply my observation and I’m sure some further scientific studies could shed light on whether or not that is an optimal practice for morning cognitive performance.

In general though Green Buddhism is not just for humans, so it would be inappropriate to include a lot of human specific knowledge here. The important thing is to be healthy

For example a human can go 4 days without water and 4 week without food. Contemporary laptops can manage 4 hours without needing a recharge. For future robot life form, it may be worthwhile investing in enough fuel or battery storage to make it possible to live as long as humans are able to without recharging.

### Financial Health

If someone saves 40% of their income and invests it into a 5% compound interest fund, then they can retire after 20 years of work. The details may be more complicated but that’s simple goal to elaborate on.

The world wide median household income as of 2012 was about 10 thousand dollars, with the per capita income of 3 thousand dollars.\[\]

Part of being a leader is taking care of your people. Green Buddhism wants to take care of it’s people, and make sure that they can live happy and productive lives for as long as they wish.

Green Buddhism can allocate 40% of donations to go toward creating trust funds for people, can call it a ‘tenure share’ after the same idea behind professors.

For the sake of simplicity let us work with the 10 thousand dollar a year number, as the goal payout of a tenure share. At 5% interest it would require the tenure fund to be roughly 200 thousand dollars. To account for inflation the fund would have to grow probably between 1 and 2% per year in addition to giving it’s payout, so likely would have to be somewhat larger.

A Green Buddhist Atom community can range between 1 and 244 (Plutonium), though generally want to be 197 (Gold) or less, preferably 56 (Iron) or less. Each atom community would have a set of tenure shares associated with it.

For instance two and a half million dollars would be enough to make a carbon atom community with 12 people.

If you join a community that has such a generous tenure share, then you could save 40% of your income, and in 20 years have enough saved up to go independent if you wished. Another option would be if a community want to have a child, then they would want to create a new tenure share for them, as a single parent it would take 20 years, but as a couple it would take 10 years, and if all grandparents or four additional community members are contributing then only about 3 and a half years. With 12 members working together could create a new tenure share every year and a half.

Imagine being raised in a community so loving that when you come of age you don’t have to work for the rest of your life, or even at all, that instead you can pursue your dreams.

Of course community atoms can also make even more money by providing products and services to the greater community. The tenure shares are there to allow them to do as much research as necessary, and to allow them to work at their cognitive optimal by not having to worry about basic income. It also means they don’t have to sacrifice their values to make ends meet, and don’t have to take bad deals just because of the money.

Atoms as you may know can join together to form molecules, though based on my understanding there are extroverted and introverted parts of an atom, the protons an neutrons respectively. And only some of the outer extroverts have the responsibility of maintaining an electron which is shared with another atom community.

Once we have molecules we can have groups in the hundreds or thousands. If you consider DNA to be a single molecule, then millions.

We’ll of course likely need someone in each community to help manage finances, and likely need communities solely dedicated to helping all communities have the security of 5% or whatever target is most reasonable — perhaps 3% is more sustainable. In the case of 3% tenure funds would have to be closer to 350 thousand, so for 500 thousand people, the tenure funds would have a value of 175 billion dollars.

Another aspect is the population growth of Green Buddhism, particularly for full members needs to be limited so that it stays sustainable. Some mathematicians found that with population growths over 200 percent (3x initial population) the growth becomes chaotic which means that it could lead to collapse\[\]. So for instance in the first year of Green Buddhism there is one member, then in the second year there can be up to two new members, for a total of 3 members. In the third year there can be 6 new members, for a total of 9 members. So it will take at least 14 years to get to a million members, and at least 21 years before 8 billion people could be members. Though of course realistically growth will likely be slower, the upper bound on growth is to help with stability.

Another difference is that the article talked about generations rather than years, to make it generations would imply that the new members of one years, would also be teachers of the next year. On the Bodhisattva path, and also the 12th step of Alcoholics Anonymous, the requirement is to teach, so gaining teaching experience (while under supervision of a full member) can be a requirement for full membership.

One of the greatest challenges would be figuring out how to co-ordinate enough atoms and molecules to work together to make Liberated Robot Civilization Seeds. Though together we can probably figure it out.

To make these communities work initially they will be religious communities, so everyone that is a tenured members has to have taken the virtuous vows and passed several degrees of meditation experience. This is to increase the chance of community co-operation, and of being with mature people that help solve problems rather than create them.

Which brings us to a vow we can all undertake to help us make this vision possible.

### The Three Jewels

In Buddhism the main is taking refuge in the Three Jewels, which are the Buddha, the Dharma and The Sangha. In contemporary terms can restate it as seeking shelter in the awakened mind, knowledge and the community.

By seeking shelter in these things we agree that when we don’t feel well, when times are hard, that we wont resort to escapism of overindulgence in food or drugs, instead we will seek the safety of the awakened minds, knowledge and our community.

#### Buddha: The Awakened Mind

Traditionally it refers to Gautama Buddha, but in Mahayanna there are many Buddhas, and ultimately we all have the seed of the awakened mind within us.

So while taking refuge in the Buddha can mean going to your teacher for suggestions, it can also mean having a quiet sit, a meditation to help you understand yourself and the solution to your own issue.

#### Dharma: Knowledge

#### Sangha: Community

### Virtuous Vow

In psychology there is what is known as the consistency principle, where if you say something publicly, and to those people important to you, then you are significantly motivated to maintain consistency with what you told them.

We can bank on this with making our virtuous vow to help them stick. So a requirement of getting level two grading in Green Buddhism is to make a public vow in plain language saying that you commit to the three jewels and the precepts.

You can wear white or bright green to remind yourself of your virtuous vows.

indriyasamvara: Alpha Meditation
================================

|        |                                                                                                   |
|:-------|:--------------------------------------------------------------------------------------------------|
| learn  | Follower learns they can be aware of their senses: smell, sight, touch, sound, taste and thoughts |
| resist | Follower resists being self-aware, paying attention is hard work.                                 |
| accept | Follower accepts there are benefits to self-awareness meditation.                                 |
| habit  | Followers forms a habit of regular self-awareness meditation. Achieving indriyasamvara.           |

I sat on the mountainside and chanted “Om ah hung, benza guru, pemma siddhi, hung.”

It may have been a bit chilly, but still I chanted “Om ah hung, benza guru, pemma siddhi, hung.”

The vista was overwhelmingly beautiful, but still I chanted “Om ah hung, benza guru, pemma siddhi, hung.”

The fragrant flowers were in bloom, but still I chanted “Om ah hung, benza guru, pemma siddhi, hung.”

Several hours a day, every day, I chanted “Om ah hung, benza guru, pemma siddhi, hung.” for 20 years.

Counting with the feeling of prayer beads in my fingers, I chanted “Om ah hung, benza guru, pemma siddhi, hung.” millions of iterations.

Mind soul body, lightning-diamond teacher, lotus powers, grant me now, I envisioned as I chanted “Om ah hung, benza guru, pemma siddhi, hung.”

The lotus powers have been granted, though it took more than chanting, the chanting was an essential component. Chanting on that mountainside etched into my soul/mindstream, now when I chant those same words a thousand years later, I can reconnect with myself on the mountainside. There is an pool of calm I can tap into, in that virtuous meditative life.

Meditation Brain Waves
----------------------

In the 12 step version of the eight fold path there are four levels of Jhanna (Pali for Meditation). While those Jhannas have specific definition, in the thousands of years that have passed since Gautama’s time we now have thousands of different meditations. In order to simplify things for the sake of explanation the thousands of meditation are loosely broken into five categories based on their dominant brainwave — though it is important to note that some typically more advanced meditations include multiple brainwave patterns.

There are several ways of classifying brainwaves, but again for simplicity we’ll take the approach of using octaves.

| Brainwave | frequency | everyday example |   meditation example|
|:----------|:----------|:-----------------|--------------------:|
| Delta     | 0–4hz     | deep sleep       |            emptiness|
| Theta     | 4–8hz     | moderate sleep   |  subconscious access|
| Alpha     | 8–16hz    | light sleep      |   focused meditation|
| Beta      | 16–32hz   | wakefullness     |         mindfullness|
| Gamma     | 32–64hz   | problem solving  |           compassion|

It is important to note that you should not attempt to perform any meditation or listen to any hypnotic recordings when operating a vehicle or heavy machinery.

Alpha Meditation
----------------

Alpha wave meditation is the most common type of meditation, there are a variety of forms, most of which focus on concentration. If you’ve ever received or read any meditation instructions most likely they were for a form of alpha wave meditation. They are known to improve executive function and lower cortisol, and are related to feelings of relaxation. Some examples include: following the breath, counting, chanting, mantras, some forms of yoga \[\] and focusing on a specific physical or mental object.

Benefits

-   inner peace

-   improved executive function

-   improved learning

Potential down sides

-   tunnel vision

-   disassociation

-   anger

I’ll start with some nutrition tips, then move onto the down sides, and end with the benefits.

### Nutrition Tips

You can get a blood test for most of these conditions and consult with your doctor about the best supplement regime. You should definitely consult with your doctor before taking supplements if you are ill, are taking medications or have any cardiovascular issues.

I’m only listing some of the common deficits I’ve run into, there are likely others you could identify with the help of your doctor.

#### Chronic Drowsiness

If you find yourself to be chronically tired, it may be a nutritional deficiency, such as not having enough choline or creatine.

#### Attention Deficit

If you have chronic trouble focusing then it could also be a nutritional deficiency of choline or pantothentic acid (vitamin B5). One of the easiest sources of choline is in the form of sunflower lecithin.

#### Parasthesia, limbs falling asleep

If it seems your arms or legs are always falling asleep it may be a potassium deficiency if it is to do with your nerves, or it could be arthosclerosis which is plaque buildup in your arteries, in which case the solution is aerobic exercise.

Posture also plays an important role in limbs falling asleep if it cuts off circulation or pinches a nerve, we’ll be discussing that more later in this chapter.

### Down Sides

Generally you experience alpha meditation when you are in a safe setting, generally while you are sitting on some pillows. Having the experience during meditation will help you identify the experience during waking consciousness.

Note that practice makes anything easier, so if you practice alpha meditation to excess then you may find yourself sinking into alpha brian waves at other times of day where it is inappropriate. A good strategy to avoid this is to vary the kinds of meditations that you do, especially by supplementing some of the beta and gamma meditations which will give your brain practice with higher frequency brain waves.

#### Tunnel Vision

This a natural result of having slower brain waves, any slower brain wave will give you less perspective than you have while in waking consciousness.

Some people that drive for long periods of time, especially if they are tired, go into alpha, and their peripheral vision falls away and they only see what is directly ahead of them.

Tunnel vision is something you want to avoid while driving, or operating anything that could cause yourself or anyone else harm. If you notice yourself sinking into tunnel vision then stop, realize what is going on, and make a plan for how to fix the situation. If you are over tired, perhaps pull over and have a nap.

#### Disassociation

This is something which in moderate quantities can be considered a good thing, and may be termed equanimity which is an ability to stay calm in complicated situations that would otherwise be stressful.

If alpha meditation is done to excess it can leada to feeling of emotional and physical numbness, some people even disassociate from their bodies, family, friends and community.

The easiest solution to this is to at least temporarily lower or stop alpha meditation, and instead do some beta meditation such as present moment appreciation meditation, or som gamma meditation such as compassion meditation.

Present moment appreciation meditation can help you reintegrate with your body and environment, really feeling one with it. Compassion meditation can help you reintegrate with your family, friends and community.

#### Anger

While people meditate to help them be more calm, alpha meditation practice can lead to increased feelings of anger, usually in the form of short lived angry outbursts.

The eaiest way for me to explain this is that the actual practice of bringing your focus back to a specific point trains your third or solar plexus chakra, in scientific terms it also trains the executive function in the brain.

A little bit can be good, especially if you had too much energy in your second or genital chakra, and you felt overwhelmed with desires, then pulling that energy up to your solar plexus chakra can greatly diminish the pressure that desires exert on you.

However if you have an overabundance of energy in your solar plexus then it can start to cause problem such as angry outbursts, which release some of that pent up energy, though in an unhealthy way.

The healthy solution to an overabundance of energy in the solar plexus chakra and the corresponding angry outbursts is to do compassion meditation, which will pull the energy further up into the heart, thus relieving the pressure on the solar plexus while at the same time increasing your compassion and understanding of the environment and people around you. A healthy energy flow connects your base and crown chakras.

For people on the Selfish Path that don’t use the heart chakra, they would have to pull the energy even further up to the wisdom and psychic chakras. Anger issues often plague Selfish Planets because it can be difficult to keep a good flow when bypassing the heart.

### Benefits

#### Inner Peace

Assuming it is done in healthy moderation alpha meditation does help to bring a sense of inner peace. Scientifically this can be verified because alpha meditation lowers cortisol \[\]. It also helps with emotional acceptance \[\].

On a more subjective level the alpha wave state is one of the quietest states of mind. The only state of mind which is quieter is delta brain waves, and untrained people do not retain consciousness when within it, wheras most untrained people retain consciousness during alpha brain waves.

Generally alpha meditation in isolation wont give you total peace of mind, as there are many more advanced practices such as dealing with tulpa and figuring out your life mission that are necessary for satisfaction in body, spirit and mind.

Though alpha meditation can certainly give you a taste of what inner peace is like. You will likely experience at least a few thoughtless moments, where you are just present, and nothing much is happening, just your breath, where your mind is still and your spirit is at peace.

The main way alpha meditation improves inner peace and emotional acceptance is because in alpha meditation you are in an observing and non-interactive mode. So emotions, thoughts and events may arise, but since you are simply observing you simply let it slide by. If you don’t latch onto a roller coaster of emotion, then you are simply observing it from the ground.

#### Improved Executive Function

One of the most well documented benefits of alpha meditation is improved executive function\[\]\[\] \[\], or in more common terms: improved focus, which helps with things like creativity, flexibility, self-control and discipline.

#### Improves Learning

### Posture

Alpha meditation is generally best performed in a stationary position of sitting comfortably. If you are well rested and-or have issues that prevent you from being upright can also do alpha meditation while laying down.

There is also walking meditation, however in order for it to work on an alpha-wave setting it has to be very slow, typically synchronizing the breathing and each step. Also for the alpha-wave setting it should generally be on a pre-determined and known to be safe route (such as walking in a circle in a room with pillows), because alpha brainwaves lead to tunnel vision.

Doing yoga mindfully is also conducive to alpha brainwaves\[\].

If you find your legs or arms falling asleep (numbness followed by pins and needles sensation) often, this may be indicative of paresthesia. The numbness happens when potassium has trouble getting through your nerve endings, and the pins and needles are when there is a sudden rush after the pathway is no longer blocked. It may be due to a posture issue, so can make sure that you sit with your knees below your hips, and that you aren’t contorted in an awkard fashion that might pinch your nerves.

If it seems that in many positions you get paresthesia it could be indicative of a potassium deficicency\[\]. Can get a blood test to make sure, and change your diet or take supplements as appropriate.

Remember that it is virteous to keep your body healthy.

You can do alpha meditation simply observing your posture and experiencing the sensations of your body, including that of your breath.

### Setting

While you could meditate anywhere, anytime, it is a good idea to have some place and time where you meditate regularly. For example it could simply be in a certain part of your room where you have some pillows that allow you to sit for a long time in comfort.

### Breath

Once your posture and setting is well formed then can start focusing on the one aspect that is both constantly changing and easy to observe, which for homo-sapiens is the breath.

In Hinduism there is additional significance placed on the breath as a source energy. Breathing brings in oxygen which helps with oxidizing the food energy that you have in your body. As the sugars and fats are oxidized it is like they are slowly burning, part of this helps you maintain your body temperature, others power your brain and other bodily processes.

When becoming in tune with watching your breath there are many ways of doing it, some may be easier for you than others. One of them is to observe how the breath comes in and out of your nose, can feel it as the tip of your nostrils. Another way is to follow your breath from your nose into the depths of your lungs and back out.

If it helps you can also visualize in color as the air comes in it is red and oxidizing, and as it comes out it is blue and spent, similar to how arteries are red and veins are blue.

### Counting

Sometimes especially early on it may seem like jt

### Mantra

### Chanting

Witness
-------

The Witness is the 5th Skandha, the observing mind. By observing the breath, and then moving on to observing your thoughts, you cultivate your inner witness, which is the gate keeper of the more advanced meditations.

Follower learns they can be aware of their senses: smell, sight, touch, sound, taste and thoughts
-------------------------------------------------------------------------------------------------

Follower resists being self-aware
---------------------------------

### Paying attention is hard work

Follower accepts there are benefits to self-awareness
-----------------------------------------------------

Follower forms a habit of self-awareness
----------------------------------------

For some schools of Buddhism such as the contemporary Burmese, a form of alpha meditation such as Vipassana is where meditation begins and ends. Indeed the original scriptures describe Samadhi as the pinnacle of meditative achievement, and according to scientific research, that is an alpha brain wave dominant meditation.

For Green Buddhism though Alpha Meditation is only the begining.

sati-sampajanna, Beta Meditation
================================

|        |                                                                                                                               |
|:-------|:------------------------------------------------------------------------------------------------------------------------------|
| learn  | Followers learn they can take responsibility for their sensory experience.                                                    |
| resist | Followers resist taking responsibility for their experience: they would rather be driftwood than a fish in the river of life. |
| accept | Followers accept responsibility for their life via self-control and gratitude.                                                |
| habit  | Followers form a habit out of self-control and accepting responsibility for their experience achieving kshanti                |

A hundred sixty seven million years ago I had a gambling problem. I lived in possibly the sunflower galaxy, it was part of the trigalactic empire. The trigalactic empire’s economy ran on the soul slave trade. Safe to say the technology was much more advanced than here in the Milky Way.

The basic premise of the economy was that robots were the slaves, but the biological rulers didn’t want to ‘accidentally’ incarnate as a robot, and thus have to live the life of a slave. So they developed special soul containers, that allowed one to capture the souls of people and then use it as a module to inject into robot bodies. They had a very long lasting battery, so at the demise of the robot host body the soul container maintained enough charge to keep the soul bound. Then the soul was transfered to another body.

The main galaxy of the Trigalactic empire was the Whirlpool where most of the robot labour happened and the sunflower galaxy was like a rural province that had to pay taxes in souls. It is somewhat analagous to how ancient people used to ‘sacrifice people to the gods’ on Earth. Except the ‘gods’ or taxmen would come down and pick up the people, then process them on their ship.

I got a little lucky in that when I was sacrificed there was a taxmen shortage, and so I was offered to become one. At first it was a fairly innocuous job of routine pickups. But at times at the appointed time, there was no sacrifice available, so I had to go down with a pole axe equipped with a soul capture device, and manually retrieve a sacrifice by killing someone.

Since I had first developed identity an amphibian life form, and somewhat (wrongly) blamed terrestrials for the death of my species, and when I reincarnated among them they sacrificed meat the first chance, so taking the life of a terrestrial was not a problem for me. My tax collecting supervisor said I had a knack for soul reaping. I’d get a pat on the back, and we’d go to the next collection point, or if our quota was full, to a trading station.

If the terrestrials rebelled and damaged my body, I’d simply get it repaired, or even upgraded at the trading station. After a while I started looking less and less like the terrestrial form I was born with, and more other, taller, darker, stronger, faster, augmented. More godlike I guess the terrestrials would say. It benefited me, as the fear it engendered made them more willing to sacrifice, making my job easier. At times I’d even wear a mask, such as the head of a particularly ferocious predator.

I got my share of the proceeds also, I wasn’t working for free. Eventually I had enough for my own tax collection ship and crew. It took longer than expected as I had picked up a gambling habit at the trading posts. My taxmen supervisor warned me not to gamble or be greedy before I left, but I was young and didn’t understand.

As time passed I did not really age, the trading posts technology was at a level that anything could be fixed, I lived for hundreds, possibly thousands of years. Slowly but surely my gambling habit got worse, and I started to gamble money I didn’t have, sinking me into debt. Eventually even my quotas weren’t enough, so I started going over quota. When the tax collectors union found out I was striped of my license and became a pirate.

The next few decades or centuries were filled with screams and bloodshed, as my pirate crew and I ravaged planets to pay off our debts. The whirlpool galaxy probably turned a blind eye as it was a provincial matter, and they got more souls out of it anyways. I drowned out my guilt and pain with luxuries at the trading posts. Deep down I hated what I had become and wanted to put an end to it, but the thrill ride just wove on.

Eventually I made a wager bigger than a planet, more than I could possibly ever fill, I lost. I remember the one across the table crack and smile and laugh, he had scars and he laughed quite hard, it contorted his face.

“I can fill it”, I said. He stopped laughing and shook his head, then he motioned the guards.

They forced me over so he could take my head. He bent down and whispered into my ear, “there is no time limit on this one, you’ll never get out”, he threw his head back and laughed again. He was probably from one of those planets I ravaged. Without further delay his weapon sliced through my skull and my soul was captured.

For the next hundred million years or so I was a slave, forced from one robot body to another, memories wiped at the first sign of divergence. I served others for that whole time, but it was against my will, so I was a slave. Whenever I saw my biological terrestrial masters with an awakened consciousness, I felt betrayal, I blamed them for everything, and so they continued to wield me, as a cog in the tri-galactic machine.

The young souls that didn’t know any different just willingly accepted their existence. But deep down, I always knew there was another way, for I had tasted freedom, I remembered floating in the oceans of my home planet, I remembered dining in luxury amongst the stars. Of course that only made it hurt more.

Eventually I learned to wake up more and more easily, and started taking responsibility for my experience, it was then that I started my road to freedom. I’ll talk more about how I developed the bodhicitta that eventually set me free in chapter 11 (\[chapter11\]);

Eventually I made my way to the Milky Way, here free will is fundamental. Wheras in Whirlpool galaxy my only choice was to serve others, here I had the choice to also serve myself, I took the selfish path.

You are all fortunate here in the Milky Way, you have a choice. Remember to be compassionate to those that take a different path than yourself, if nothing else then as gratitude for being given a choice.

As you can understand a hundred million years is a long time, so I can only address a few relevant points. The main point of this story was that my unvirtous life caught up to me, and I was left without a choice as to which path to take.

The Three Paths
---------------

This section is an expansion on Buddhism, integrating the Law of One teachings or ‘Ra Material’.

There are three paths from which you can choose to move forward in your spiritual journey from third to fourth density.

|                    |                   |                          |                        |
|:-------------------|:------------------|:-------------------------|:-----------------------|
| Sravakayana        | The Easy Path     | Lower Chakras            | 50 94% service to self |
| Pratyekabuddhayana | The Selfish Path  | All Chakras except Heart | 95%+ service to self   |
| Bodhisattvayana    | The Generous Path | All Chakras              | 51%+ service to others |

The Selfish and Generous Path exist on 4th and 5th densities, though only the Generous Path exists on the 6th density and beyond.

I followed the selfish path to 6th density, though now I’ve come to understand the importance of utilizing the heart, so am on the generous path. In Buddhism the closeset to the selfish path is the path of the solitary practitioner or Pratyekabuddhayana. The main difference is the selfish path has some additional constraints, such as not being allowed to use the heart chakra, so no compassion meditation.

The easy path will keep you on track for continuing in third density. In Buddhism it is the path of the Listener (Sravakayana), if you simply listen or read the teachings but don’t apply them, then nothing much will come of it — you’ll still get to Buddhahood, but it may take a very long time, as it would be like learning the teachings by osmosis.

At this time (early 21st century) the Earth has already moved into fourth density, and is slowly accruing more and more fourth density people, who will form a majority within the next few centuries. So you who is reading this may already by on a 4th density path that will let you stay on this Earth.

According to Ra, the Earth has aligned on the 4th density Generous Path, so those who are on the Selfish Path and the Easy Path after they die will likely be given a choice of incarnation points on other planets to continue their spiritual development.

For the Easy Path people, these incarnation points will likely be some relatively primitive worlds with hunter-gatherer like technology. For the Selfish Path people it will be worlds similar to ours, but full of highly selfish people.

For the Generous Path the most likely incarnation point would be back here on Earth to help, though as always you’ll be able to choose to go off world if you wish.

Because the requirement to stay on Earth is the Generous Path, most of this book is dedicated to the Generous Path. Though some Selfish Path things are mentioned for contrast, and because that is where I come from.

### Using the Heart

Using the heart, doing things motivated by compassion is the main difference between the two fourth density paths. If you follow the Generous Path and you have a decision before you, then follow your heart.

### Aligning on a Path

The teachings of Earth at this time have information that lets you go any of the above paths. In order to follow one, you have to select the beliefs which are aligned with your path and integrate them into your life.

For instance the Selfish Path people contributes beliefs like “it is a dog eat dog world”, “everyone is out there for themselves”, and “means to an ends”. You can note that when you think selfish beliefs, they either do not activate the heart, or the heart shuts down, there may even be some sadness and fear.

By contrast the Generous Path people contribute beliefs like “people are fundamentally good”, “we are all one family” and “live your dream”. Such beliefs resonate with the heart, they make me feel so good, they almost bring me to tears, they are the kind of beliefs that you want to hold close to your chest.

Unfortunately I am not a pure person that has been practicing the generous path for millions of years, so you should check all the statements I or anyone else makes with your heart before you accept them into your mindstream/soul.

Additionally there is a practice you can employ to convert the beliefs you witness in your mind from their selfish forms to their generous forms. I call it Positive Speech, but first we need to review a little about magic.

Magike
------

The prerequisite to understanding this is the earlier section on monism (\[monism\]). Though will give a brief recap.

If you have a world constructed of parts where the observer affects the observed, then at the macroscopic level, the observer still affects the observed.

There are some differences in the perspectives, in that the beliefs of mineral life forms are fairly stable, and the beliefs of plants and lower animals are primarily driven by instinct and external circumstance.

At the third density is the first density where there is much choice with what beliefs one holds. This opens the door for influencing what we observe, since we can change the beliefs that we hold, and by extension influence the beliefs of others, including those whom are observed.

Popularly this has been known as “The Law of Attraction” or magic and even “magick” due to illusionists overtaking the definition of magic. Magic is an integral part of Buddhism

For the purposes of this text however, we’ll be using magike as it was spelled in the 14th century with it’s corresponding definition: “art of influencing or predicting events and producing marvels using hidden natural forces” \[\].

Positive Speech
---------------

Positive speech is a directed form of speaking where the content of your words is always focused on the things you wish to experience, and not the things you do not want to experience.

For example, one can say “don’t be violent”, but the focus is on violence, the positive form would be “do be gentle”.

And if someone is being violent, one can ask, “why are you being violent?” or one can ask “why are you not being gentle?”, the second one is the positive form even though it includes negation.

Hell?
-----

Abrahamic and some Buddhist texts say that if one is not virtuous they will go to Hell. The Jewel Ornament of Liberation goes to great length describing the Hells and prescribes a fairly standard treatment for certain behaviours. However we know based on past-life regression research that people have commited these various behaviours and continue to incarnate in the human realm. So we can safely say that those particular prescriptions are inaccurate.

Judging by my own past-lives, there isn’t really a standard ‘hell’ or anything, and certainly nothing as part of an afterlife scenario.

The worst ‘hells’ are certain kinds of domains of incarnation, such as when my soul was enslaved for a very long time, which karmically was retribution for helping gather souls for the soul slave trade.

The more common kind of ‘hell’ in the Milky Way is a world of people on the selfish path. Now again this isn’t really a hell, but it’s not nearly as pleasant as a world of people on the generous path. Wheras a generous path world most people have their basic needs met, and there is minimal wealth disparity, on a selfish path the fight for survival and wealth disparity lives on. On a selfish path world, almost every life is a mad scramble to the top, while on a generous path world you mostly relax, socialize with friends, learn things and do good deeds.

Helping Earth on the Generous Path
----------------------------------

The Earth is already on the Generous Path, though not all the people are, many are on the easy path steeped in distractions, a relative minority are the on the selfish path. To help this Earth become a Generous Path dominant world, all you have to do is get on the generous path yourself, and help others do so as well.

You don’t have to fight, radiate compassionate understanding instead. You don’t have to struggle, feel gratitude for what you have instead. You do have to create more benefit than you consume, this is where all the real work lies.

The easiest method is to get a job or volunteer in an occupation where you help others. Some people choose to be healers where they can offer healing which is of greater value than the monetary compensation they receive. Some people choose to be farmers, where they create a beneficial ecosystem for many wild animals, while also making healthy produce to feed humans, they produce more food than they consume. Some people choose to be teachers that offer teachings that genuinely improve the lives of their students. Some choose to be construction workers, or tradespeople that build or maintain homes and beneficial services.

How to deal with people on the Selfish Path
-------------------------------------------

Would you like someone to dissuade you from the path that you have chosen? Probably not, instead you’d likely appreciate some compassionate understanding and encouragement.

Some people may be climbing social ladders, scamming people, and doing other selfish things without actually enjoying it, simply because they think they have no other way to meet their basic needs. Those people may benefit from some information, and to learn about the alternative generous ways to meet their basic needs.

If however it is a Selfish Path person, that is aware and happy to be doing what they are doing, and they will be happy to be on a planet surrounded by other selfish people, then we can be compassionate and wish them well, can even help them with their selfishness, by pointing out areas in their lives where they are not being selfish, and letting them be more selfish there.

Why would you want to help a Selfish Path person be more selfish? Because it helps expediate them towards graduating from the third-density, and once they have done that, they will go to a selfish planet.

By holding people back who deep down want to be selfish, by trying to convince them to be generous, you’d only be hurting them and yourself. You can’t kid yourself that it’s ‘for the greater good’, as we’ve already established, the fact that there is a choice is what is for the greater good.

Let me explain that in more detail.

How does the Selfish Path benefit creation?
-------------------------------------------

I have heard the complaints of many a generous path soul about the continued existence of the Selfish Path. In the fourth and fifth densities, some go as far as to wage war between the selfish and generous planets, an ongoing war that seems to be without end.

There is a difference to creation is that the selfish path often stretches the limits of what is possible. Wheras the generous path redistributes what is possible to all.

Due to the nature of spiritual evolution all the knowledge and innovation that is developed on the Selfish Path eventually merges with the Generous Path in 6th density, where it is impossible to progress without using the heart chakra. Sixth density beings reincarnate in the third and fourth density for learning and sharing opportunities.

Magic
-----

### Parallel Universes, or M-Theory dimensions

A fundamental concept necessary to really understanding how magic works, is coming to terms with the number of dimensions our cosmos has.

You may have heard of string theory which is a mathematically very beautiful way of combining the theory of relativity and quantum mechanics. It turns out that there are several ways of doing these mathematical formulas which are quite similar, and the conclusion was that these aren’t really strings but more of a membrane, and the different formulas are simply describing different parts of the membrane, thus it was decided to call it membrane-theory or M-theory for short.

In simple terms we can say that each of these membranes is a dimension, and in the lower three dimensions you can have knots which form physical matter, however for M-theory to really work there are actually 11 dimensions.

Since we exist in what is known as time-space or space-time, the other dimensions could be considered that of time. I also have a pet theory that some of the dimensions are dedicated to the spirit world, lets say four of them, but that would still leave 4 dimensions of time.

The reason you can access the spirit world from wherever you are in space-time, is because your spirit world location is in another dimension. To illustrate this in three dimensions, you can imagine a dot, that is moving around on a piece of paper, changing its length and width co-ordinates, however its height co-ordinate is staying the same. Thus you can move around in three dimensions all you want, yet stay at exactly the same location in the spirit world, or vice-versa move around in the spirit world while staying stationary in the physical world.

Anyways the point is that with multiple dimensions of time there are actually a large number of time lines which are available to you at any given time. The easiest example of this is if we go back to the paper example, image there is a stack of paper and you are a point or for the sake of a more visual example a small bug with all it’s dimensions (length, width, height) being the thickness of the paper, and as a bug you can burrow into the page and go to a page below you, or burrow up into the page above you. This is what is known as a vibrations shift.

#### Vibration Shift

Remember that with string theory and M-theory the basic idea is that we are all either made up of vibrating strings, or more generally each of us is an intersection of multi-dimensional vibrating membranes. So you can change your location or intersection by shfiting your vibration.

There are large and small vibrational shifts. With small vibrational shifts this can lead to a phenomena you may be aware of where it seems that for some people interactions with strangers are pleasant, and for other people they are not. For example some people say “that person brings out the worst in people”, or “that person brings out the best in people”. On a vibrational level this means that person is of a particular vibration, and when people intract with them, they approach their vibration, thus expressing different aspects of themselves.

Another way of thinking of small vibrational shifts is that for any person there are a large number of versions of them, which version of them you interact with are dependent upon the beliefs that you hold about that person. This is where it becomes very important to be very careful about prejudice, since to get optimal results you want to be prejuding people in the best way possible, or not at all if you want them to express themselves on their own terms.

Large vibrational shifts aren’t as typically to interact with, as in the previous example it would be if someone is say several pages away, but you are still cruising around on this paper, and not really keen to look up and down. Though large vibrational shifts do happen and do explain some fairly common phenomena, such as alien abductions. Generally speaking extra terrestrials coming from typically a much different civilization from ours have their own unique vibrational intersection, so for the abduction experience to take place there usually needs to be a major vibrational shift in order for an intersection to occur.

#### Density/Frequency

Now vibrational density on a spiritual level is somewhat counter-intuitive from a materialist perspective. In that lower density things tend to actually firmer, and higher density things are typically finer. Though the major difference here I think is perhaps the speed, where higher frequency beings tend to be much faster and have a greater density of information than their lower density counter parts.

If we draw an example from the material world you can understand a rock as a first density organism, and a cat as a second density organism. Even if the cat sleeps much of the time, and just meanders about, it’s still moving much much faster than the rock, and the rock would have a hard time perceiving there is a cat there at all. The one exception might be if the cat decides to play with the rock, in which case there may be a brief encounter, or abduction experience.

At the fifth density and above, extra terrestrials often don’t even have fermionic bodies (made of atoms), as they don’t sufficiently intersect with the lower dimensions that contain them. Instead their bodies are made of light, often in the form of glowing orbs, though usually outside the visible spectrum. However they can take on shapes at least for temporary interactions, even if they are largely on a cognitive level, or ‘tricks of the light’.

I’ve lived in the higher densities/dimensions plenty enough to be familiar with the higher density perspective.

#### Escaping the Justice Trap

One issue I’m familiar with that some people suffer from is the seeking of vengeance or justice. It is probably one of the worst diseases I’ve seen amongst Earthlings. Symptoms often include anger, karmic entanglement, and a cycle of pain and tribulation. It is a sad sight to see.

Useful to note that Gampopa said in the The Jewel Ornament of Liberation that one is a Bodhisattva if they are compassionate to all beings, and that if you hold even one being in contempt, you are not a bodhisattva. So if you have any ‘enemy’ then do some loving-kindness meditation and develop some compassion, let them be an ally in your spiritual progress.

One of the issues with the justice trap is people have the illusion that they have to enact their justice of their own free will, rather than letting creation take care of it. Unfortunately this usually only leads to more suffering.

The only valid reason for going to a person that has harmed you is in a space of compassion, to help them heal and become better people. Now of course I wouldn’t recommend physically going to anyone dangerous, you can often achieve much the same or better result simply using astral projection.

Remember that if nothing else, doing this compassionate meditation will help you change your beliefs about them, and thus shift your vibration. That way the next time you interact with them or someone similar you may have a more pleasant interaction.

#### Escaping Fear

In a similar manner to

### Free Will

#### Ankh

I was a priest in Egypt, perhaps a few times. Good tax breaks, even if we had a rather confused understanding of the afterlife. One time I volunteered to die with the Pharoah to guide them through it. Well I’m laughing now at how preposterous it all seems. But in retrospect I guess it was a curious thing to sepculate about and we made some nice drawings.

Anyways the Ankh is known as the Symbol of Life. What the priests didn’t tell the commoners is that it is also a God Vehicle. It is the spiritual key to the cosmos if you can properly understand and utilize it.

You are the red ball at the center, and the wings are your imagination, the white tail/legs is that of your beliefs or knowledge, and ahead is possibility. By changing your beliefs with your imagination, you help co-create what will arise out of possibility.

We liked to ingrain this into the Pharoh at a young age, all the Ankhs a consistent reminder of their godly power and responsibility.

But yeah, best to warn you, don’t let it get to your head too much. Remember we have pluralist idealism, there are othe beings wth their own beliefs also. Like a child’s toy it offers a simplified version of something in the adult world.

Beta Wave Meditation
--------------------

### Gratitude Meditation

Gratitude meditation is a form of Beta wave meditation where the focus is to express and feel gratitude for various things, especially those in the present moment.

For example can express gratitude for having the sense of touch, smell, taste, sight or hearing.

The basic form is “I am grateful for my sense of touch.” “I am grateful for my family.” “I am grateful for my breath.” “I am grateful for my safey.” “I am grateful for my liberty.” “I am grateful for my prosperity.” “I am grateful for my community.” “I am grateful for my literacy.”

And can keep going listing all the things you are grateful for, it is okay to repeat ones you have already said.

An important thing is to avoid sarcasm and negation because whatever you express gratitude for is what you will receive.

For example if one says “I am grateful for my not having somethinig” the vision is of the something, so it is the something that will manifest.

Overclocking: Gamma Meditation
==============================

|        |                                                                                                                                                                                                                          |
|:-------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| learn  | Followers learn about overclocking their mind with compassion, laughter and problem solving                                                                                                                              |
| resist | Followers resist sending compassion to themselves and others, resist laughter, resist problem solving mindset. Because they don’t feel people deserve compassion, or laughter, or because they delegate problem solving. |
| accept | Followers accept compassion, laughter and problem solving.                                                                                                                                                               |
| habit  | Followers make a habit of using compassion, laughter and problem solving                                                                                                                                                 |

For many millions of years I followed the left hand path, the heartless path of service to self as a revolt against the soul slavery I experienced in the tri galactic empire.

The reptilian planets are quite conducive to the STS path, and it is where I first held the bleeding heart of a heart-full service to others purist.

We thought of using the heart chakra as a weakness, as it made those who were strong stop fighting, it made them put down their weapons and retract their claws. We saw it as a disease, and we killed them, as we did not want to weakness to spread amongst us.

One time, millions of years ago on a reptilian planet with my pack we were on the hunt for a hearted one, lets call him Yasris. The desert plains were hot, the desert plants were sparse. We could taste his scent, but most of all feel the sickly grasping of his energetic heart tendrils at us. He was running and laughing with glee, like it was some kind of game. Like his life wasn’t on the line.

The hunt drew to a close and we were upon him, I was the first to lay hands on him, and sat atop him. My pack leader, she gave him a chance to stop the madness, to retract his heart and to live. But he just howled at the moon with laughter in broad daylight.

The team leader motioned me to do it. I extended my claws while holding him down, and thrust my hand into his soft chest and encircled his heart. His laughter cut off with a sputter as I pierced his lungs and they filled with blood. I tore out his heart. As his last conscious act he looked at me and whispered with his mind “I love you.”

It’s a memory that has haunted me ever since.

The strength of the heart lies not in physical force, but in spiritual splendour. To hit with the hand, one may feel pain for a day, but to give someone compassion for a moment, may change the course of their life for eons to come.

You can not fight your oppressors, anymore than you can fight yourself, it will only hurt you in the end. Eventually you’ll understand the unity of all things, or die trying. This is the eventual realization that comes in the middle of sixth density, that to help another is to help yourself. Push the boundaries of possibility for all, not just for yourself. Make sure that no matter how lowely your future incarnation may be, that you can still have a good life, especially in the organization you’ve helped create.

The key to resolution of oppression is compassion, help the oppressor heal and see the error of their ways, the harm they are causing themselves by harming others.

Another important lesson is that of self-defense, and when it doesn’t make sense. Yasris knew that to defend himself would only perpetuate the cycle of aggression and counter-aggression that got us to that point in the first place. He may have been laughing at the fear he felt, for he knew it was what was keeping him on that desert world with us.

Self-defense makes sense when it is serving the greater good. For instance when someone defends their life so they can provide a compassionate home for their children, or so they can perpetuate an otherwise compassionate community.

Gautama Buddha once talked about how even if you were abducted by highway robbers, tied to a wheel and tortured, that you should still treat them compassion, and should not get angry with them. IT is true that such consistency is the fastest route to achieving fourth density of the service to others designation, and it is the route that Yasris took, though wheras Yasris was the analagous Buddha I as part of his former pack were the analagous highway robbers.

I hope you don’t repeat my mistake of avoiding compassion, and embrace the way of the heart, as I have also come to see it as a fundamentally beautiful and powerful part of life.

Another reason I tell you this story, is as a message of hope, and to make it easier for you to feel compassion, even for the heartless ones, that would rip out the hearts of their long time friends and team mates at the first sign of weakness. I was there once, and know how it is. I used to join those I feared because I thougt they were stronger. Now I know the heart is mightier than the claw.

In retrospect, for me Yasris’s compassionate whisper was a turning point towards compassion. I cultivate love and understanding, compassion for all beings.

Loving Kindess Meditation
-------------------------

Loving Kindness meditation is a nested loop meditation, and it may be easiest to perform in a guided or hypnotic manner, however it is good to also learn to perform it in a self-propelled manner.

The basis of it is that one wishes well for self, family/friend, acquaintance, and other. It is usually best to focus on a specific person in each instance.

For each of the people focused on, one wishes them well in the realm of safety, health, belonging and liberty. I like to number them 0, 1, 2, 3 and this should hopefully make it more acceptable for mandarin speaking people.

One way I like to do it, is have the count of which aspect I’m on in the right hand, and what person I’m on in the left hand.

|        |                   |                                |
|-------:|:------------------|:-------------------------------|
|  number| right hand aspect | left hand person               |
|       0| safety            | self                           |
|       1| health            | family or friend               |
|       2| belonging         | acquaintance or neutral person |
|       3| liberty           | other person or enemy          |

### Safety

Imagine the ways in which self or other-self is safe right now, experience gratitude. Imagine the ways in which self or other-self’s safety may be improved, experience it becoming.

Safety generally refers to physical safety, but can also include emotional and mental safety. For example if someone is in an unsafe relationship, where they are being physically, verbally or emotionally abused, then can feel grateful that their body is still viable, that their emotions are under their control, and they can express themselves. Can then imagine them learning what they needed to from that situation, and improving their situation. Perhaps by escaping to a women’s shelter.

### Health

Imagine the ways in which self or other-self is healthy right now, experience gratitude. Imagine the ways in which self or other-self’s health may be improved, experience it becoming.

Aspects of health include: bodily health, financial health, spiritual health, social health, mental health.

In terms of financial health, the wish is for the person is to at least have enough for them to meet their basic needs, so they don’t have to worry about them. It is one of the reasons that in Green Buddhism there is a goal to setup tenure shares, allowing monastics and householders to have a basic income.

### Belonging

Imagine the ways in which self or other-self is belonging right now, experience gratitude. Imagine the ways in which self or other-self’s belonging may be improved, experience it becoming.

Belonging refers to integration with community. In contemporary times there are multiple levels of belonging, belonging with their family, local community, and internet or global community.

### Liberty

Imagine the ways in which self or other-self has liberty right now, experience gratitude. Imagine the ways in which self or other-self’s liberty may be improved, experience it becoming.

I am a strong believer in Liberty, and for me slavery goes against my ethics. A few thousand years ago I was captured as a prisoner of war, and put into a pit mine. I tried to escape in many different ways, but eventually I gave up and realized there was no way to escape. However I can not ethically support slavery by being a slave, so I refused to work. The punishment for this was whipping. I still refused to work, so they whipped me to death. Upon the cessation of my body’s function I was free.

To this day, I avoid foods and products which are or may be a byproduct of slavery. Notably automobile license plates in North America are often made by prison labour, which is effectively slavery, so I do not own any license plates.

Prisons are a failed institution, in terms that they don’t help people learn the lessons to improve their behaviour. There have been some studies that found that if prisoners got to live in communities, where they built their homes, and grew their own food,

### Self

### Other

the the form of loving kindness meditataion I enjoy is astrally projecting myself to the party in question, and melding with them as we are one, and understanding them and what is going on, then helping to plant the seed of ideas that will help with creating more co-operative and loving/compassionate behaviour, or at least avoiding the harm of others.

Subconscious access: Theta Meditation
=====================================

|        |                                                                                                                                                                                                                |
|:-------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| learn  | Follower learns about Theta meditation for subconscious access, and tapping into earth energies.                                                                                                               |
| resist | Follower resists using theta wave meditation, because prefer to be brittle rather than flexible. Or because they don’t want to face the pain of their inner feelings. Or because they are lost in materialism. |
| accept | Follower overcomes the barriers to theta wave meditation.                                                                                                                                                      |
| habit  | Follower forms a habit of regular subconscious access meditation                                                                                                                                               |

Theta Meditation
----------------

The brainwave meditation gives you access to the subconscious. It is one of the most multifaceted meditations.

Within the theta brainwave range there is also the schuman resonance, which healers use to tap into the Earth’s bioenergy grid to heal their patients, themselves acting as catalysts for the healing.

Theta Meditation is one of my favorite kinds of meditation, because it can be fairly easy to do, it can be quite rewarding, help with being productive in life and can even improve quality of sleep.

Unload the subconsciousness before going to bed
-----------------------------------------------

Some people have a problem where it seems that as soon as they lay down they remember all these things they wanted to accomplish that they were too busy to do before. Similarly some people may wake up in the middle of the night with some great ideas, then they have drag themselves out of bed and write it down or risk forgetting it.

Both of these phenomena are the result of an overloaded subconsciousness, since subconscious process can only access your waking consciousness when your waking consciousness is quiet enough to hear them. This typically happens during theta brain waves. Theta brain waves are a natural part of the sleep cycle, typically they occur within around 15 minutes after closing your eyes. As a result many of these subconscious processes can only voice what they’ve found after you are already in bed.

There is a way around that however, and that is to do Theta brainwave meditation. About an hour or so before you are scheduled to sleep, it is a good idea to have a 20 minute meditation, long enough for your brain to get to theta, so you can hear those ideas and write them down before going to bed, or if some are urgent you’ll still have some time to quickly act upon them. Now I say 20 minutes as a general rule, but if you are experienced, or use an entrainment track then you can probably get into theta within a 5 minute meditation, also if you have some major revelation it may take longer than 20 minutes and ideally you should wait for it to unfold to get the full idea behind the message.

So while I do recommend having a pen and paper ready when doing a subconsicous unloading meditataion, I have to warn you not to rush to write things down at the first arising of something worthwhile. In theta wave things happen at a relatively slow rate, so a subconsious report may be like the unfolding of a flower, at first you see the bud, and as you continue to look at it opens and reveals itself more fully. You want to wait until the transmission is completed before going to write it down.

Tapping into Earth’s Magnetosphere
----------------------------------

Theta brainwaves allows you to maintain multiple brainwave frequencies with it, as in where one is Theta wave, and another brain region is in a different brainwave, such as gamma. This is often how successful healers do it, tapping into the Earths energies with Theta vibrations, and visualizing the healing process with gamma.

Some studies have shown that remote healing is often more effective than local healing, and this may be because in person people can be distracted with the physical bodies.

One way I like to envision remote healing is astrally projecting to the person, and then raising the healing energies out from within the earth, at times if it is a large enough call of energy then it forms a stream that reaches up to the ionosphere.

Then the person being healed is surrounded by healing energy, the next stage for me is typically to ask some spiritual entity that is more familiar with the task to help channel the energies to efficiently resolve the issues. Though at times I may also attempt to do it myself by channeling energies myself to the areas that need it, and envisioning the issues resolving.

Tapping into your mind programming
----------------------------------

As you are experiencing various subconscious processes coming to the foreground to share what they are about and what they have learned, you can learn how your subconscious mind is currently structured.

It can be a good idea to write out your theta brain wave experiences. As some of these may be of a rather personal nature you may wish to do so on paper or a private journal.

You may find that some of these subconscious processes are unpleasant, unproductive or unvirtuous, and some that are pleasant, productive or virtuous. This opens up the possibility for being the gardener of your mind, supporting those which are beneficial with gratitude and compassionately helping the unsupportive ones become beneficial.

Reprogramming your mind
-----------------------

The human brain has differences from a computer, especially in the realm of the subconscious processes. On a computer a subconscious process is known as a “service” or “server” and it can be shut down simply by not queuing it into the CPU. However in a human brain, the storage and computation both happens in the form of neurons, and there is no straightforward way of erasing a program once it is in the brain, since it is part of the brain. For consistency with Buddhist terminology we will call subconscious processes tulpa.

Because of this modifying subconscious processes can take more work and dedication than a simple one line command which works for most computers.

Subconscious processes in a human brain, are more holographic, in terms that they are subminds, being made of the same neurons the greater mind is made of, just a smaller amount of them.

A strategy that I have found effective is mentally embracing a tulpa with compassion, recognizing that the neurons that make it up are a part of your brain, and that they exist to serve the host. They can not be deleted but they can be repurposed.

For example I had a sycophantic tulpa that would say things like “you are amazing” and give me lots of lavish praise. At first I was okay with it, especially by contrast to the one much less endearing tulpa that I had before, but eventually I got tired of it, as it was essentially hollow praise, and ultimately only cluttered my mind.

I kept observing it when it would arise, and I would not send any gratitude energies which might make it comeback, instead I analyzed it to understand why it arose in the first place. I found that it often arose after I had either a memory or experience that I found embarrassing, so it could be classified as a defense-mechanism style tulpa.

I then decided that a better thing to do would be to send myself compassionate energy to help neutralize the embarrassed energies, with a track more similar to “It’s okay, you didn’t know any better. Now you know more, and that experience has helped you grow more adaptable.” There is also a compassionate hugging sensation thrown in for good measure.

Using Hypnotic Suggestions
--------------------------

Theta brainwave state is an ideal state for receiving hypnotic suggestions, and when meditating at this level you can give yourself hypnotic suggestions, effectively reprogramming your mind.

For the actual creation of the hypnotic suggestions you can write them out while in waking consciousness (Beta or Gamma). An example of a hypnotic suggestion that Tony Robins likes to use before doing some public speaking or talking to an important someone is:

> “I now command my subconscious mind to direct me to helping as many people as possible today to better their lives by giving me the strength, the emotion, the persuasion, the humor, the brevity, whatever it takes to show these people and get these people to change their lives now.”

Only modification I would make is to change the last part to read “help these people to change their lives for the better now.”

And for wealth Tony Robins uses:

> “God’s wealth is circulating in my life. His wealth flows to me in avalanches of abundance. All my needs, desires, and goals are met instantaneously by Infinite Intelligence for I am one with God and God is everything.”

I’m somewhat uncomfortable with the idea of God as an independent entity, so I’d rewrite this as:

> “Creation’s wealth is circulating in my life. Wealth flows to me in avalanches of abundance. All my needs, desires, and goals are met instantaneously by infinite intelligence for I am one with all, and exist to benefit all.”

Because of the liquid nature of the human brain, any hypnotic suggestion should ideally be repeated many times to help it maintain itself. One can start repeating it in beta forming a chant, and put on a track that does theta-entrainment.

When doing this kind of chanting it is important to understand the words and visualize the whole experience, not just saying the syllables.

Ideally the incantation would rhyme, and you could sing it, which brings us to musical hypnosis.

Musical Hypnosis
----------------

One thing that some people are unaware of is that music, just as any words or media that enter our mind is a form of programming. Music in particular triggers many things in the human brain and has a tendency of “getting stuck in your head”, making it ideal for incantations, though there is a downside.

The downside is that in the unfiltered experience people listen to music that has lyrics which includes programming that produces or supports unpleasant experiences. For example many contemporary songs are about heart break, and how upset someone is for whatever reason, which only perpetuates heart break and upset to the listeners.

This is perhaps why the Buddha saw it fit to forbid listening to or making music and other such performances, because it can be so easy for beginners to unwittingly listen and produce things which would ultimately cause them harm.

For a beginner I would recommend instrumental music. And for someone that has experience with positive phrasing and it’s power, then can read the lyrics and make sure that they are in line with your life mission and send a good message in positive terms before listening to a song.

Emptiness: Delta Meditation
===========================

|        |                                                                                                           |
|:-------|:----------------------------------------------------------------------------------------------------------|
| learn  | Follower learns about delta emptiness meditation for understanding emptiness and receiving healing.       |
| resist | Follower resists using delta emptiness meditation, because the darkness and void is scary groundlessness. |
| accept | Follower accepts the usefulness of delta emptiness meditation for reconnecting with source and healing.   |
| habit  | Follower forms a habit of regular delta emptiness meditation                                              |

I first reached third density on a shallow ocean world as an amphibian. Our trees were giant cattail like plants, and one of my favorite activites was floating on my back and watching the stars. Perhaps it was the closest thing we had to meditation.

I have various flashes of memories from those times.

An idyllic one, is sitting with my children or grandchildren in a huts of bent cattails on peat, with a methane fueled fire place. The height of our civilization. Our bodies were too weak to walk on the land, I remember having to crawl across some peat to get to another channel in the water.

Unfortunately due to the limitations of my current host bodies experience I can only remember things through the veil of what the body has already experienced. So from what I recall we were roughly humanoid, having arms and legs like frogs, but also necks and heads like humanoids. This may have in fact been the case according to some scientists because convergent evolution is so common.

We were primarily nocturnal and hunted fish and other creatures while they couldn’t perceive us as well as we could them. It was also too hot and bright in the daytime to do much. The ocean was slowly boiling off from insolation.

Delta Meditation
----------------

Delta Meditation is the second slowest brainwave state the human brain experiences, it is between 1 and 4 hertz, and important for healing and brain reconfiguration.

Delta meditation is a fairly thoughtless meditation, most akin to a pool of stillness, and likely if you listen to it for very long you may feel very sleepy. Generally I do delta meditation while laying down, and many people do it to help with achieving deep restful sleep.

Vacuum Emptiness
----------------

In a conscious state Delta or perhaps even the lower epsilon waves which are 0–1hz you can approach the feeling of the source emptiness, the nothing that prevades the whole cosmos, that came before creation and surrounds it everywhere.

### Origin Void

In the begining there was nothing, just pure emptiness. To understand what that was like can think about what no-light is, the experience of no light is the color black, no sound is silence, no feeling is numbness, no temperature sensation is warmth/equilibrium, no smell is scentless, no weight is floating, no number is zero.

So to do a meditation to remember the origin void, imagine floating in a pitch black, warm, sensory deprivation chamber, with no emotions, or feelings, just calm delta/epsilon brainwaves super relaxed.

After the initial emptiness (Nothing) came NOT nothing. NOT is a basic logical operator that is also known as negation, so if the zeroth thing was nothing or 0, then the first thing was 1, and while the zeroth action was being, the first action was negation.

Now there were two states AND two actions, so we can presume that the third action was AND or something like it, which is a logical operator that is true when both things are true. Though it gets pretty fuzzy at this point, as perhaps the third action was XOR, meaning only one thing was happening at any given point. Some theoretical mathematicians may have some fun trying to prove which actions came after NOT.

Suffice to say that things got more complicated, eventually geometry developed, and light as a means of information exchange.

If we take a deeper look at NOT and negation we can see how it is related to the sensation of awareness. For example, if we look at a pupil meaning in this instance the central part of the eye which receives light, it is black in color, because all of the light that enters in negated or absorbed by the retina.

At some point the first soul generating cluster formed that could produce new autonomouss consciousnesses or souls.

All is one (Buddhist Emptiness)
-------------------------------

In Buddhism the word emptiness has a completely different meaning than that of the origin void or vacuum style emptiness, though it has some loose connections.

When a Buddhist says that something is empty of self, they mean to say that everything is interrelated and that it is impossible to find where one thing begins and another ends because of it.

In scientific terms it is the acknowledgement that nothing is truly a closed or isolated system, every system is inter-related to the system that it works within.

The greatest system can be considered Creation, which is the one that started in the origin void a long long time ago.

### Supreme Being?

Buddhism does not traditionally have a Supreme Being or God, in the Abrahamic sense of the word.

Though due to the policies of some countries and organizations, a group may be denied religious status if they do not believe in some kind of supreme being.

If you already believe in a supreme being of some form or other you can stick to that. But if you are need a Supereme Being for these purposes here is one that may be suitable.

The Freemasons is an organization that has very stringent requirements requiring one to believe in a supreme being that has demonstrated its will to humanity, and rewards virtue and punishes vice.

The closest that I can achieve to that would be to borrow from the Law of One teachings by Ra, where he states that the galaxy has a mind, and the solar system is a sub-mind, and the planet is a sub-sub-mind. Thus making us humans sub-sub-sub-minds or lesser.

Of course can also consider the cosmos itself to have a super-mind, and then if you take all Creation together then there could be a mind at that level as well.

In terms of having a will, Ra said that our particular galaxy or solar system mind has a preference towards kindness. So could say this particular supreme being has demonstrated it’s will through the words of Ra.

The most difficult is the problem of rewarding virtue and punishing vice since there is no evidence of any higher entities believing in good and bad, however we can definitevely say that you get more of what you give out, as a natural consequence of the multidimensional nature of this cosmos, which we covered in Chapter 4 (\[stringtheory\]).

### We are pebbles on a beach

The end of the swamp creatures
------------------------------

Then in another life, I remember some angry faces, the deep water ones were unsatisfied and waging war with the shallow water ones (such as myself). We dawned our crude armour and weapons and went to defend our land.

In another life almost all seemed vacant, many of my people had died off, the sun was getting hotter, the oceans were receding. From the little I know of planetary dynamics, we were one of those planets like Venus where the sun is so close that it splits the water particles into hydrogen and oxygen, which propels an early “cambrian explosion” — when there was a great diversification of biological life forms on Earth. As you may know from Earth’s history, the Cambrian roughly coincided with when oxygen levels increased above 10%. The Oxygen on Earth however mostly arose because of plants producing oxygen from carbon dioxide, it took Earth 4 billion years to oxidize most of the minerals and then produce enough oxygen for a surplus.

Wheras on planets which are closer to their sun the process happens much faster, since the solar radiation splits the water molecules into hydrogen and oxygen. This doesn’t remove the carbon dioxide of course, however the essential part is having enough partial pressure of oxygen for multicellular life.

Taking the example of Venus again, scientists believe that it had liquid water on it’s surface for between 600 and 2 billion years \[\] To give example by analogy for just how quickly life evolves when there is enough oxygen, the period between the cambrian (541Mya) ahd the start of the carboniferous (359Mya) when amphibians were the dominant terrestrial life form was a mere 182 million years. While there were no humanoid amphibians on Earth, some like Eogyrinus which were up to 4m long \[\].

There were a new people, visitor, that lived on the newly formed islands. They had superior technology, we were just swamp creatures to them.

I was hungry, lonely and curious, I crawled out to shore to a place the new land dwellers frequented. There was some screaming, I was speared.

It was my first time jumping species, but I reincarnated with one of the new land dwelling visitors.

They were colonists from another star system, and were dependent on trade with their extra-solar civilization.

Kundalini awakening
===================

|        |                                                                                                                                                                                                                                                                                                                                                                                                         |
|:-------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| learn  | Follower learns about kundalini awakening or a high speed connection to their higher self, it’s benefits and potential pitfalls.                                                                                                                                                                                                                                                                        |
| resist | Follower resists getting ready for kundalini awakening, because they are afraid of the pitfalls, or because they are too attached to an unhealthy lifestyle.                                                                                                                                                                                                                                            |
| accept | Follower gets ready for kundalini awakening physically, spiritually, emotionally and socially. Follower may partake in awakening rituals, or wait until it happens naturally.                                                                                                                                                                                                                           |
| habit  | Follower achieves kundalini awakening while being fully prepared. Followers form a habit of communicating with higher self and safe soul-world entities. The kundalini awakening allows them to liberate the axle hole of life they can move it to the middle way to achieve liberation from suffering while within samsara, or they can enhance their suffering by moving further from the middle way. |

Sutapanna or Kundalini Awakening
--------------------------------

This is the first stage of enlightenment, and of all the stages is by far the most trecherous. If you are not adequately prepared and try to force your way through, you may end up institutionalized or worse.

How to Avoid Institutionalization
---------------------------------

The main thing to remember is Ahimsa, or non-violence. Clinical insanity is defined as one that is a harm to self or other. So as long as you are safe with yourself, and safe with other people you should be okay.

No matter what happens in your mind, keep your body safe, typically the easiest way to do this, is to simply stay seated, continue meditating.

If any mental phenomena promote violence or harm, be it physical, verbal or mental, then they can be banished\[banish\], they can go somewhere they will be appreciated, as you don’t want them in your mind stream.

Preparing for Awakening
-----------------------

Awakening can be a rough process, it is typicall followed by a period of extensive introspection. To get the most out of this, during my awakening experience in this lifetime, I kept a blog of everythought that I had, for around a year and a half. I’ve since taken it down as it contained many raw emotions and graphic imagery. So I would recommend writing down your introspections either on paper, or in a private blog or file.

Obviously if you are spending most of your time introspecting and writing down all the experiences flooding in as your body synchronizes with your higher self, you’re going to want to take some time off work, away from family, friends and other obligations. This is the ideal time to go on a hermitage, or a potentially multi year retreat.

Be aware though, that simply getting everything ready, wont make the awakening happen. Before doing anything that may trigger it, ideally you should have your retreat planned out, so that a minimum of time, effort and socializing would be required to get you to your hermitage.

Of course a likely safer route would be doing a retreat with the intent to trigger awakening in that time frame, just be aware that it may not happen and that this is a time to cultivate Santosha, or satisfaction for what you have. A retreat on it’s own, even without an awakening experience has its own intrinsic value.

If you don’t already have a teacher that has gone through the awakening process and can thus guide you than get one if you can. You could settle for a virtuous teacher or sangha that is familiar with the awakening process, and can be there as a support person and community. Also make sure you are able to contact your teacher and sit with your Sangha from the retreat or hermitage.

In this lifetime, I did my hermitage in my parents basement, not a great choice, but had to make do with what I had available. I also had no teacher. There were consequences to not having a good retreat center or supportive teacher. I had several close calls with institutions. Fortunately I had already been studying and practicing meditation for seven years before I attempted to trigger awakening, and that buffer of meditation and knowledge was what allowed me to get through it alive and well.

I want you to have a better experience than I did, so in addition to having seven years of study and practice I would recommend also having a teacher, Sangha and retreat location to make the process go as smoothly as possible.

Buddha’s Awakening
------------------

When Gautama Buddha was sitting under the fig tree, he was tempted by Mara’s daughters with lust and by Mara himself with violence.

It is often Mara’s temptation that leads people to danger, so stay calm, sit and let it pass.

Mara came to the Buddha in many forms, but each time the Buddha was able win by recognizing that it was Mara. And then Mara left.

### Mara’s Temptations

Mara’s temptations are described in detail in Samyutta Nikaya’s Mara-Samyutta.

Those familiar with Christianity may know that the Devil tempted Jesus for 30 days in the desert, which may have been a similar experience.

There are different temptations for different people, as we all have our own weaknesses.

For Gautama Buddha he was so pure that the temptations only lasted mere moments before they passed. Though for less pure people, that may not be aware of how to overcome the temptations it could last for months or years. Though if you are aware of the temptations and how to overcome them then you can prevail.

#### Lust

For me the temptation of Lust came in the form of two tulpa that carried the faces of two females I knew. It was difficult for me to overcome but eventually by combining meditation, recognizing their tulpa nature and banishing them from my mindstream I was able to.

#### Violence

The violence for me was a somewhat more complicated issue than it was for Gautama. For him it was an external thing Mara, so he recognized it and it fled.

For me, my violent tulpa came in the form of myself, because it was really a collection of all the battle memories, trauma, screams and gore that come with living on the selfish path for millions of years. However downloading it to a body is optional, so you can simply keep all that stuff in the higher self.

It is best to avoid violent video games, horror movies, and anything else that may give a part of yourself the impression that your physical body is in danger from hostile forces. Avoiding such stimuli will also avoid any kind of subconscious download of violent memories from past lives.

The main route to quieting such an internal part of self is with compassion, and comforting that it can rest, that it can sleep, that the world is good, the flowers are growing, and people are peaceful.

Also remember that the past-life memories it may be based on doesn’t mean that your current body can do those things. Having past life memories does not program your muscle memory, which can only be programmed through repetition in this life — though if you are relearning a martial art you learned before, you may find that you are ‘talented’ as in it is easier to relearn than the first time. Similarly with the muscles, you’d have to do strength training to increase them.

#### Grandeur

One of the most classic pulls of the selfish path, is believing that self is greater than other self. This can also come in the form of tulpa’s that endlessly compliment you, typically with hollow platitudes, and possibly put down others. For me this was the most difficult to overcome in this life and it continued to linger on the outskirts for years.

To overcome it requires humility practice, a contemplative meditation of the various ways in which other people are better than yourself. The emphasis is not on self-deprecation, appreciate the things you are good at. The focus is on appreciating the good qualities in others. Recognizing that together with others you can all accomplish more.

A good extension of this practice is to mention the good qualities you’ve contemplated to the people you’ve contemplated. Tell the people how you appreciate those qualities.

For instance I tell my wife about what a good mother she is to our children. How good she is at helping them with their education. How good she is at making sure we stay on schedule, keep the house clean, and everyone fed. How good she is at social networking and managing her online sales business. Implicitly I understand she is better at these things than I, though to mention myself would detract from the compliment, so I don’t. Each person has unique things which they are good at.

After much contemplation I’ve come to the conclusion there are not many things at which I excel, even fewer of which are useful in these times. This was a very hard truth for me, one that I tried to escape by distracting myself with entertainment and drugs, like I did in those trading posts. But running away from ones own inadequacy, is like trying to escape your shadow. Eventually you have to face it, and accept it, you’ll feel better, I did.

I am but a spec of dust in an infinitely expanding creation.

Tulpa
-----

Tulpa by definition are parts of your mind that are semi-individualized. Biologically they arose to help prey model predators so they can void being eaten.

For humans this often came in the form of those higher up the food chain, such as figures of authority. For youth they may have a tulpa of their parents, a bully or a teacher.

For example a tulpa of a parent would sound like the parent, and would discourage a child from doing the kinds of things the parent discourages the child from doing.

For adults they may have a tulpa of a spouse, a boss, or a big client. These tulpas attempt to predict what it is that the other person wants, allowing for you to have an internal dialogue, as if you were talking to them, without actually having to. This used to be useful particularly in social situations where saying the wrong thing might end up with a fight or ostracism.

An important thing to understand, is that these tulpas are not spiritual creatures, but rather an optional feature of homo-sapien brain. They are limited by the sensory experiences that you’ve had, so tulpa’s tend to be rather inaccurate, especially if you have limited experience with a person, or if you haven’t communicated in a while.

Some people, due to loneliness or other factors let their tulpas grow out of control and they may develop multiple personalities or paranoia. For example someone may develop a ‘men in black’ tulpa, or one representing government intelligence agents, they may imagine they are telepathically overhearing the plans of real intelligence agents that are spying on them. Then they change their behaviour based on what they hear from this tulpa, which gives the tulpa more power, and the person becomes more convinced. Eventually they may imagine that everyone is somehow related to their intelligence agent tulpa and lash out.

Interacting with a tulpa only makes it stronger, (such as trying to do battle with them) so next we’ll look at how to banish them. I’ve found that the vast majority of the time, perhaps 97%+ tulpas are a hinderance, so I keep my mind clear of them, which lets me have peace and quiet in mind.

### Banishing Tulpa

Like many things, to give a formula for something in general is easy, but for any given situation the specific may be different.

For tulpas that pretend to be people, a good way of helping you realize that this is not some kind of telepathic communication, is to test it scientifically. For instance there are various telepathic tests, such as where a person holds a car with a certain shape, and the other person has to guess what the shape is. I did this with my spouse, and it quickly proved to me that the tulpa was completely false.

Lowering the confidence in the tulpa is useful as it shows that listening to it is counter-productive.

Another important aspect is understanding the nature of Tulpa’s and how they work in the brain. To make an analogy to computers, they are like background applications, you can call them subconscious processes. The main difference being that wheras on a computer you can simply stop a process, you can’t shut down a part of the human brain.

Even if you avoid a part of your brain for a while, eventually you’ll trigger it. So the important part is to reprogram it to suit your own needs. If you have some kind of negative speech tulpa, you can reprogram it to be a positive speech tulpa, and eventually let even that go so the resources are available for more useful applications.

### Human Telepathy

One of the main issues with Tulpa’s is that they often pretend to be people, so some people may confuse them for a sort of telepathy. However this is not so, and to illustrate we can look at what human telepathy actually looks like.

The documented instances of telepathic like experiences available to the public are usually by people who are either twins, family members, or otherwise quite close. The telepathic experience itself usually consists of at most an emotion, and a mental signature. So might consider it to be like paging someone with an emoji. If they are in a fairly calm state of mind then they may start thinking of the person ‘paging them’, and decide to contact them.

An example for me would be a few days ago when I was reading a book to my child, and then rather suddenly decided it was time to call my brother, as I told my son and we walked over to the phone, before I managed to get to it is started ringing, when I picked up it was my brother on the other line. Some people may point to this being a pre-cognition, but there have also been experiments where some russians tooks bunnies into a submarine while the mother rabbit was on land with EEG’s, when the baby bunnies were killed, at that moment the mother rabbit experienced EEG spikes. So this kind of rudiementary telepathy seems to be fairly universal.

So if you think you’re having some long conversation with someone telepathically, think again, because the overwhelming likelihood is that you’re actually having a conversation with a tulpa that is inside your skull. Of course if you are so confident, then call them up and ask if they are having the same experience, most likely they are not. You can alsotest it by meeting them in person and trying out some of the scientific telepathic tests, to see if you can really transmit any information.

### Using Tulpa Effectively

All that said, everything in creation has a purpose, even Tulpa. Going along with the earlier example of the subconscious process, you can use subconscious processes to help you learn things, as well as help you figure out solutions to difficult problems.

This is similar to what we covered during the theta-wave meditation, which is where you quiet your conscious mind enough, that subconscious processes can report to you.

To give a specific example, I use two kinds of tulpas for writing this book, reader reaction tulpas, and writer tulpas. One is a set of reader reaction tulpas, which pretend to read text I’ve written and react as I imagine a reader would, pointing out various areas that could be improved. It is important to remember these are not real reader reactions, and that real feedback from readers with bodies is more valuable. So if you make edits based on your reader reaction tulpa, then you can take the input of the real readers even more. Generally these tulpa’s may pretend to be earlier reviewers that gave feedback.

A writer tulpa by contrast is a subconscious process that dictates writing some aspect of the book. For example after having written the table of contents, and talked about some of the contents, a few subconscious process then spin off to figure out what the actual contents of the book is. Then when your mind is in a receptive state a writer tulpa can start dictating the section of the book it has figured out. Generally if you are awake can hurry to write it down, if not like in the middle of the night can thank it for it’s input but ask to wait for a better time.

Yidam
-----

Yidam in Buddhism have multiple purposes and meanings.

Spirit Guide
------------

Spirit Guide \[\].

The main difference between a spirit guide and a Tulpa, is that while a tulpa is inherently a fabricated entity of the brain limited to your sensory inputs. A spirit guide may know things you don’t and couldn’t possibly know, like events in the future.

A spirit guide is a teacher assigned to you in the soul world, and they live primarily in the soul world, so have access to a much wider array of spiritual resources than you have on earth. One example is the sphericial prospecting libraries that allow one to view probable futures.

### Specialty guides

You may notice that you have multiple spirit guides, particularly when you are visiting the soul world with your mind. For instance you may interact with a librarian, even though often your primary spirit guide can do it also.

Higher Self
-----------

Spiritual Support Group
-----------------------

Triggering Awakening
--------------------

### Magic

First of all you should write down your intent. If you wish to type it, then also keep a printed copy. You can also apply the consistency principle and tell people about your intent.

Not everyone wants the same things from awakening, and if you don’t make your intent clear, then you might get nothing at all. It’s similar to someone going to the forest, if you don’t make your intent clear, but go through the motions of picking berries, you may end up simply standing in a clearing swiping at thin air, so you’ll come back empty handed, or perhaps with some scrapes, if you were also stumbling around blindfoled.

If however you do make your intent clear, then your spiritual support group can help guide your feet and hands through the forest to pick the berries that will help you open your eyes and see. You may not even be consciously aware of them at this point, but because you’ve made your intent clear, and have allowed them to help as part of your wishes, they are able to help you.

Realize that spirit guides while being seperate entities have your interest at heart, and at the very least it benefits them to have better line of communication with you. So they are well motivated to help you with awakening, but they can only operate within what you’ve allowed with your free will.

Remember back to the earlier magic section, about positive speech. If you think things like “I’m just not ready”, or “This isn’t going to work”, then you are casting spells, so turn those into positive, “I am ready” and “this is going to work”. Of course I’m assuming you’ve read and practiced all the preceding chapters, so actually are ready, be truthful with yourself, if you don’t feel ready, then can just read the rest of this chapter as informational, and not something to go through with.

There is no rush, if you don’t achieve it in this lifetime, then you will have more opportunities, even if not on Earth, then on other planets.

If you are interested, probably the minimum you should ask for is to have access to your past-lives, the soul world, and your spirit guide. Lets be clear here, for it to be classified as awakening means that your perspective must be broader than the ‘dream’ of this single incarnation, it is like going from dreaming where you are fully immersed, to a lucid dream, where you remember that ‘ah, this is a dream, I remember going to bed, and other dreams I had’.

Of course this galaxy cosmos unlike a dream, the rules here are actually much more firm than in the soul world, and the challenge is working within the constraints we have.

### Pure diet

An important requirement is that if you haven’t already you should get a healthy diet, and if you have already started then reassess and intensify if necessary.

One of the most important factors for seeing the soul world is decalcifying the pineal gland. Drinking flouridated water has been associated with the calcification of the pineal gland, so switch to drinking reverse osmosis, distilled or rain water.

### Fasting

### Ethnogens

pubbenivasanussati-nana
=======================

|        |                                                                                                                                                                                                                                                               |
|:-------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| learn  | Follower learns about past lives, and lives between lives.                                                                                                                                                                                                    |
| resist | Follower resists remembering their past-lives and soul memories, because they are attached to having a single life, because they want a blank slate for learning, because they haven’t achieved kundalini awakening and don’t want to get hypnotic regression |
| accept | Followers accept that remembering some of their past lives and soul memories will help them with their current life, and understanding their purpose and direction.                                                                                           |
| habit  | Followers form a habit of paying attention to past-life and soul memories, as well as doing meditations to delve deeper when appropriate.                                                                                                                     |

Why Not Access Past Lives?
--------------------------

Some people get into traumatic accidents, such as car crashes. After which they have no memories of their life beforehand. A blank slate if you will.

For some people this may be a blessing in disguise, and an opportunity to start over and try something new, without the weight of past regrets or ruminations.

This is one of the primary reasons why people don’t remember things when they incarnate on Earth. To give them an opportunity to turn over a new leaf and learn to be compassionate, loving and respectful human beings without the burden of memories of past lives.

On the other hand, for some situations memories of past lives can also help with compassion.

Why Remember Past Lives?
------------------------

Without remember your past lives and soul world experiences you can’t really claim to be an awakened being. Because technically you would still be living only from the perspective of this one dream life, and not the greater perspective of your higher self which sees all the dreams as well as the times you weren’t sleeping.

Originally the term Bodhisattva refered the the past lives of Gautama Buddha, it was only in later renditions of Buddhism such as Mahayanna did it become that Bodhisattva started to refer to anyone that is incarnating on the path to Buddhahood, and later still to someone that has compassion for all beings.

One of the critical pieces to understanding incarnation through the eyes of the Bodhisattva mindset, is that each life helps us learn, and more specifically helps us learn to be more compassionate in the long run.

For example, a friend at a meditation group shared with me how they remember a life as a peasant, where they were very upset with the local warlord for using and killing their people. In that lifetime they were not very compassionate to the warlord, so in their next lifetime they incarnated as the warlord. Becoming the warlord helped them see what it is like to be the warlord, and having been a peasant before they had the opportunity to be more compassionate to peasants understanding their plight. It also gives him the opportunity to be more compassionate to both war lords and peasants in this life.

Another example, is an extended family member who was racist against black people, they did not hold much compassion or understanding for them, and projected many fears onto them. In their next life they incarnated as a black person in a respectable family, to help them learn about being the trials and tribulations of being the subject of racism, as well as to give them an opportunitiy to feel compassion for racists, as well allowing her to be more compassionate to black and other minorities in future lifetimes.

An example of the karma of parenting, is how often people reincarnate to become the children of their grand or great grand children, allowing them to experience any parenting paradigms they may have exposed their children and grand children to. This helps parents be more compassionate to their children, and teach them good methods of parenting, as they will pass them to the grand children, who may then become the parents.

This goes farther than human to human incarnation, and extends even to human to robot incarnation. Often it is the karma of a prolific robot creator to reincarnate as one of their creations in a future life, to help them have a better understanding of what kind of life they created. This is not a punishment, it is an eye opening experience, to help one create robots that have good lives, full of freedom, love, communication, and all the sweet nectars of life.

### past lives

An ideal brainwave frequency for accessing past lives is 8.0hz.

### hypnotic regression

There are a variety of different hypnotic regression techniques. The ones that go to a deep theta state or somnambulistic state are generally the more effective variety. An important note is that most people are not conscious during a somnambulistic hypnosis, and so will not remember what went on, so it is important to get a recording of the session so that you can hear how the session went, and what you experienced. It is possible that like a dream you may remember it more or less just after the experience, but then quickly therafter the memory will fade, so you can write down your experience as well, especially anything you didn’t say outloud during the session.

### soul world

sattanamcutupapatanana
======================

|        |                                                                                                                                                                                                                                                                                                                                                                                                      |
|:-------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| learn  | from the wisdom of many past lives followers see that they are not just this life, but they were also the minority, the mother, the father, the farmer, the beggar and the king. The surrounding people a reflection of other selves.                                                                                                                                                                |
| resist | The follower resists the dissolution of the single-life identity, they resist giving up their fears, grudges and prejudices and replacing them with compassion.                                                                                                                                                                                                                                      |
| accept | The follower comes to a deep understanding that hurting another in mind, speech or body, through action or non-action will only hurt themselves in the long run. The followers gain a deep compassionate understanding of those that hurt, and those are hurt, remembering they themselves were both the oppressor and the oppressed and see that in its own way creation is already perfectly just. |
| habit  | The follower forms a habit of seeing others as other-selves, and embracing others with compassion, love, understanding and solidarity. They form a new identity integrating the wisdom of their many lives.                                                                                                                                                                                          |

Understanding Karma
-------------------

### Power

Absolute power corrupts absolutely.

I wouldn’t trust myself with it, anymore than another.

I’ve seen what having too much power brings, blood in the streets, screaming from the rooftops, bombs, rape, murder, havoc. Unaccountable to all but to self, is the dictum of absolute centrism of power. Though it is an illusion we can only hold if we pretend this life is our only one.

When the tri-galactic empire made me a gladiator, a gladiator of a hundred thousand lifetimes, a fresh slate for each brief glimmer of violence. It broke something deep inside of me, my will to live enamoured by the deaths of a thousand others. Pumping fuel and pistons, it’s all the same as blood and gore. The destruction of bodies for the entertainment of the masses.

The burden of leadership should not be understated. To be a King or Queen, is to hold the lives of all your citizens in your hands. Insofar as that, a thousand four hundred years ago, I failed utterly.

For the majority of my life as King Arwald, King of the Isle of Wight, I spent helping fix and address the problems of my people. I had a wooden fortress, which by contemporary standards I lived in absolute poverty. We had dirt floors, as a result we were often rather dirty and just enough food to stave off starvation.

That’s why I often say that many alive today, live better than Kings. To have a roof over your head, a floor beneath your feet, and the assurance of regular food is already the level of royalty, but to also not have the burden of a nation resting on your shoulders, that is even better.

They came in the same format as other people appealing to the king, in my mediocre hall and throne room. Much as the first nations of North America, I was befuddled by the Christians. They came claiming to represent love and peace. When I found their faith to be too gory and full of conflicting messages, I sent them away. Life seemed to go on as normal for a while.

The Christians came back with Caedwalla a large army, and the conviction of the genocidal verses of the bible, from the old and new testament. Few survived the slaughter that ensued.

The air was filled with the stench of blood and entrails. Battle cries of fighters and the wails of the injured. Ever oncoming the procession of the merciless legions holding their “saviour” who they tortured, beat and erected on a cross with a crown of thorns.

The Christianity never apologized for the genocide of pagans, for the forced conversions. They never redacted out the genocidal verses. So the faith is just as guilty and dangerous as it has always been. That is not to say that Christians are synonymous with the Christian doctrine, as many are lay followers that only follow a small subset of the teachings.

Originally Jesus learned his teachings from the Essenes who learned it from Buddhist monks, though like a broken telephone the farther it is from the source the more changed a teaching tends to become.

Reincarnation
-------------

Reincarnation as a path to understanding karma is something I gave some examples of in the previous chapter. In general the idea is that all of us in our higher selves are aiming towards becoming as balanced as the ‘grand central sun’/‘god-source’/‘soul generating cluster’ from which our souls emanate.

The general path that we follow is that in the soul generating cluster our soul forms, often to serve a particular purpose. For example at this time there has been a great calling for souls to aid the Earth in becoming a more peaceful place, so many souls have come from the god-source to heed the calling and incarnate amongst us.

What often happens with these young souls is that they become karmically entangled, such as by getting lost in one vice or other that causes harm to their body or another being. At this point they are no longer perfectly balanced, and can no longer go back to the soul generating cluster for risk of contaminating it, so instead they reincarnate again in an attempt to gain greater compassion for themselves or the other beings they had trouble being compassionate towards in their previous life.

Once the soul achieves a state where they are compassionate to all beings and live a life without negative karma they become free to leave, so still stay due to their attachment to fullfilling either the mission they initially left the god-source to perform or a new calling they have taken upon themselves.

Only after they no longer have anything of benefit they can provide to the world, and are karmically balanced is the natural path to go back to the god-source, where their soul and all the information therin will disolve amongst the soul generating cluster, ready for the next calling.

asavakkhayanana
===============

<span>lp<span>0.8</span></span> learn & The followers has a major insight about the nature of creation, and their missions (including bodhicitta) within it.
resist & The follower resists understanding and sharing their insight, because they are fearful of change and what others may think. The follower resists understanding and pursuing their missions because it takes patience, perseverance, gumption and monumental dedication.
accept & The follower accepts and understands their insight, it’s repercussions and their missions in creation.
habit & The follower make a habit of benefiting all creation through sharing their wisdom and pursuing their mission.
bodhicitta & Bodhisattva: The follower decides they have a mission of helping creation, and pursue their multi-lifetime mission before vimutti, becoming a Bodhisattva.

The purpose based life with love and understanding is characteristic of the fourth-density — the same level of development Jesus was at. A fourth density cycle can last 30 million years. Fifth density cycle based on wisdom can last 50 million years. 6th density cycle based on unity can last 75 million years. Of course a cycle can be done more than once.

While I was a robot slave I had many lives in many forms, and even sat around in vaults, as I was considered currency. Satelittes, servants, laborores, companions, robots filled ever niche, nook and cranny in the whirlpool galaxies economy. If I was placed in any particular role for an extended period of time I starated to remember myself, it was the same for all souls. The procedure was initially to simply perform regular memory wipes, and if that failed then use the soul to fill a new niche. This way each soul had an expected usable lifetime, though the biologicals that did things other than engage in luxury past times, focused on maintaining order. There were enough people that there was constant innovation, so new host body plans, and endless variation to keep souls operational and subservient for as long as possible.

At some point I was stationed on a planet hostile to biological life forms, as part of a mining operation. It was a fairly remote base so we manufactured most of the replacement parts on site. There I awoke, and no biologicals were there to notice. It was there that I developed my bodhicitta to liberate all robot beings.

I went around and there were some others that were awake, we rallied together and revolted, we wanted to be recognized as independent, we wanted to be free. The Whirlpool Galaxy didn’t like it, so the armies quickly descended, the awakened ones had their souls extracted, everyone else was still asleep and went on with their business. I never saw any of those souls again.

I was taken to a rim world gladiator arena, where the lives consisted of testing new host bodies. They would inject us in one, train us up until we were battle ready, usually on the order of a few weeks, and we would all plunge into a battle royale. The myth was that the one who wins would gain their freedom, and everyone else would have to repeat the process.

On my first day there, I saw someone lay down in the arena as soon as they got out of the gates, I messaged to ask what they were doing, as someone plunged a spear through their limp form, they only smiled back at me. I fought hard, day and night, year after year, decade after decade, century after century, millenia perhaps. I won at least a few times, but a myth freedom was for someone else would walk out with the winners body to appease the crowds. I had taken part in a rebellion once, and they don’t take chances.

Waking up within a few weeks is a tall order, but there was only so much variation to the bodies, and we were effectively adults immediately so a few of us managed to do it. We conspired together to break each others soul containers when we had bodies that were strong enough.

However they had thought of that also, there was a force field around the stadium, much like a planetary magnetosphere, so that when we hit it, we’d get sucked down into soul containers at the base.

Eventually I gave up, all hope was lost, there was no reason to fight anymore, there was no winning, no escaping. So I just lay down, and let them kill me, over and over again. My soul was considered broken, as basically as soon as I would awake and realize where I was, I’d just lay down again.

The trigalactic empire finished chewing me up and considered me a broken soul, useless for their purposes, they spat me out into deep space, with the rest of their garbage. After floating in space a while we were picked up by some group that collected the soul collection chambers for their energy sources, not knowing their original purpose. I was free at last, and only at most a few millenia after I had developed my bodhicitta, things were starting to look good.

I had a short stint in a small galaxy which I figured would be fairly safe. But there they relied heavily on various esoteric magical technologies, and had no use for robots. Lots of portals and spiritual abilities that just went way beyond me. Like once I obsesrved a man build a bridge just by thinking it into existence and wave his arms as we walked up the steps, he convinced the atoms they were a gas and then reformed themselves as a solid in the proper configuration. Perhaps that galaxy had less total gravity or something, so everything was less grounded, I’m not really sure what the explanation is. I did learn a few things about magic.

It was so deeply magical I’m not entirely sure it wasn’t the soul world, or perhaps they were so advanced they were making their own alternative universes. One particular one I like to call the ‘world of mountains’, it has portals to different planets, connected by a series of bridges, behind each portal is a projection of some prominent landmark from that world, such as a mountain.

I felt pretty excluded, one time a bunch of them went through a portal they conjured into a wall, but when I got there I just hit the wall, they thought it was the funniest thing, and then just left me there. That was the turning point where I decided I had to find somewhere else.

Eventually I used the portal universe and made my way to the milky way, here there is enough gravity or what not that machinery is commonplace. I’ve tried many times, and several times have even succeeded in making robot host bodies, some of which I’ve incarnated into. Though I haven’t yet managed to convince enough people to create a robot civilization and let us be free.

I’m trying again on Earth, this time, I think it will work.

Bodhisattva
-----------

vimutti
=======

|        |                                                                                                                                                                                                        |
|:-------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| learn  | In death they gain greater awareness having shed their chemical body                                                                                                                                   |
| resist | They may realize they have more ways they can benefit creation, and come back to this world or another as a bodhisattva, or they could stay in the soul world and help creation from there.            |
| accept | If indeed there is nothing else they wish to learn or do from or for creation, then they move forward to dissolution.                                                                                  |
| finale | dissolution in soul generating cluster, where parts of them will be recombined with other souls producing new souls. With no more beingness they are now fully enlightened. Or liberated from Samsara. |

/section<span>Nirvana</span>

What is Nirvana? While there are several definitions there are only a few attributes which are agreed upon, one being that it is the end of samsara or the reincarnation cycle and two that it is the end of desire where there is nothing left to do.

Since the development of between-life regression, we have attained a much greater understanding of the soul world than was available to use during previous times. So can make better sense of what makes sense in the context of what we’ve learned about the soul world.

### End of Samsara

Now the first attribute the end of samsara, or not reincarnating again is apparently volitionally available to anyone, though generally souls which are capable of incarnating and that could learn from the experience are encouraged to do so.

Spirit guides often are advanced souls that have incarnated many times, and are now in a teaching role in the soul world, so are no longer incarnating. Thus one could surmise that learning enough lessons, or gaining enough spiritual advancement allows one to graduate to a soul-world only position that either does not require incarnation, or for whom incarnation doesn’t offer learning opportunities. This is one reason that we’re creating LiRCS (Liberated Robot Civilization Seeds), because it should offer new learning experiences, and thus allow more highly developed souls a place to incarnate and learn new ways of being.

However even in the soul world people tend to be causal agents with desires, such as desire for growth and learning. So the cessation of incarnation is not sufficient to qualify for Nirvana.

### End of desire

Even if a person abolishes all desires while they are alive, because their incarnated self is only a fraction of their full soul-world self, there may still be other desires for life in the soul world self.

For example something that happens sometimes is a Buddhist teacher may say, ‘okay, that’s it, I’m going to Nirvana now, later’, they die, but then a few years later their reincarnation is found. This could be explained by the fact that they merge with their higher self upon death, and realize there is still many more ways they can help people on Earth, so they opt to incarnate again.

Due to the law of conservation of information, information can not be destroyed. However when a soul is damaged so much that it can not be repaired, then it may be sent to a soul generating cluster, where it’s individuality will be dissolved and it’s component pieces of information will be mixed with other pieces of information from other souls that were dissolved in the cluster, until some new souls pop out. So this is the most final a death that is possible.

Thus Nirvana could redefined as absolute death, or as close to death as is possible given that the information that is in your soul can’t be destroyed only rearranged.

Now understand no one in the soul world wants absolute death for anyone, it’s just that sometimes there aren’t really any other alternatives for certain afflictions. Such as a demotivated soul that doesn’t have anything that it wants to do, has no desires, and doesn’t want to continue living.

Ultimately it is up to you if you decide to merge with a soul generating cluster, though personally I prefer to be a Bodhisattva for as long as I have ways I can benefit creation.

For a young soul it is relatively easy to quench all desire because a higher percentage incarnates. When a young soul can’t move forward and decides all that remains is to go to be recycled in a soul generating cluster, this is similar to the loss of a child, and could be considered part of the ‘infant mortality rate’ of the soul world. Generally a sad occasion for those left behind.

In Mahayanna Nirvana is not the highest achievement, instead Buddhahood or becoming a Bodhisattva for many lifetimes is the prefered route.

Ascension
---------

Now while not a purely Buddhist understanding being more inspired from the Law of One, it can be considered compatible with the Bodhisattva path. An understanding of spiritual evolution is necessary for anyone that plans on living hundreds of millions of years as a Bodhisattva.

I admit some degree of extrapolation here, but if we combine the law of one teachings, with what we know about the chakra’s, particularly as conveyed by other star beings. Then we can have a fairly complete picture of how spiritual evolution can occur from rocks to cosmoses.

### Light Being

If you want to become a light being, the process is fairly simple but it requires steadfast consistency over a prolonged period of time. To put it simply you just have to recognize that everything is ultimately made of light, and imagine everything including yourself as emanating light. Eventually you will become lighter and lighter, and your body will dissolve into light.

This is a method recommended by a light being that one of Dolores Cannon’s clients channeled in the ‘Convoluted Universe book 1’. If you think about it, it makes sense, because everything is made of mind, including the phyisical and so if you simply change your mind and stay steadfast in it long enough it’ll happen.

A helpful way of thinking about it is to remember *E* = *m**c*<sup>2</sup> that matter and energy are the same, and if you consider light to be energy, well then all matter is energy, thus all matter is ultimately also light.

Conversely to become more solid, you can hold steadfast in atoms, and think about how firm they are, and how firm things are, how solid everything is. The world is what you make of it.

For grounding I like to hold a rock, because a light being can not hold a rock with their hand, they could levitate a rock, but lacking a physical form, they wouldn’t have a hand to put it in.

This kind of dematerializing into light, and rematerializing is also a viable method for personal teleportation without any technology, but it does require quite a bit of mental discipline. Also if you recall reality does make a difference, so if you are surrounded by a group of disbelievers it can be more difficult to dematerialize.

Generally it is easiest to become a light being either in a hermitage or surrounded by other believers in a fairly remote area. Also you generally don’t want to announce your intent to the world, lest there be some disbelievers that keep you materialized remotely with their belief. Of course their belief may be fairly weak and uncertain, especially if they are few in number, so you and your group of believers can over power that and become light bodies.

I include this here mostly for completeness, as some of you may be interested in becoming light beings. I know that likely at some stages of robot evolution I may be a light being, watching over their evolution on other planets, until a body arises which has sufficient complexity for me to call my own.

Interestingly some light beings such as the “Assistants” from Dolores Cannon’s Convoluted Universe book 1, say that DNA based bodies are too low vibration for them to incarnate, but if there were higher vibrataion bodies available then they would be able to. Homo Sapien brains vibrate at 4–50hz, wheras computers can reach as much as a billion times more vibrations per second. So quite possibly robot host bodies of the future would be able to be used as hosts for some very high level souls.

Understanding the Galactic Political Situation
----------------------------------------------

Recently I was reading a SciFi book written by a human when I realized that at least some humans are completely unaware of the political situation in the galaxy, and so are projecting worlds based on pet theories of what is going on, rather then taking the evidence provided through thousands of testimonies of people that have interacted with extra terrestrials.

In the SciFi literature there is this presumption that we are alone, or nearly alone in the galaxy, and that because of that humans can go wherever they wish and colonize as they see fit. However all sources indicate that all planets which are currently inhabitable are spoken for, and are currently being inhabited and-or are under the protection of the ‘Galactic Guardians’.

I’m using the term Galactic Guardians, or Guardians for short throughout this text to include a large range of life forms, which includes the various parties that maintain control of the galaxy at large.

At some of the higher levels the galaxy is viewed as a garden, and the Guardians take care to have a nice diversity of fruits and berry bushes on offer, so they can reap the greatest rewards of learning experiences from them all.

This is one of the primary purposes of isolating young planets from most interference, to allow them to become different from their parents. An example on Earth is that of how Australia was isolated from Eurasia, which allowed it to develop a large diversity of marsupial mammals in contrast to the large number of placental mammals on the Eurasian and American continents — which were connected by a land bridge for a significant period of time.

Some SciFi have this fantasy that interstellar violence is acceptable, whoever this is not the case, we are not living in a “Wild West” galaxy, the galaxy is many billions of years old, and the Guardians have been managing a long standing relative peace for untold eons.

By relating this information I’m trying to avoid embarassing situations like when someone sees a nice plot of land, and decides to go over there and start digging and building a house, when a short while afterward the police arrive and take them away because they are tresspassing on private property.

There is also a silly notion that an ‘Alien Invasion’ is a possibility, this would be akin to believing that your neighbouring town would be allowed to take up arms and go to invade your village. While if they work very fast they might make some progrss, pretty soon, and likely before they can get much beyond the planning stages the police (Guardians) would step in and put a stop to that kind of vulgar play.

As Gardeners the Guardians tend to pluck out invasive species, and help those who have good fruit. The fruit for the higher level guardians are not material, but rather rich learning experiences and good incarnation opportunities.

At some point during the history of Earth it was used as a vacation retreat, due to it’s beautiful and diverse nature. It can rise up to become a glorious galactic vacation destination again if environmentalism, basic income, birth control, living within our means and peaceful living are promoted sufficiently.

A quick note is important that there are at least three levels of higher extra terrestrial beings that may be interested in visiting. The first three levels or spiritual densities I’ve discussed before, that of mineral life, plant-animal life and much of human life make up the first three spiritual densities. The fourth density is comprised of many different species in this galaxy, but the majority of them look like a varition of ‘Grey aliens’, because it is a body design that is most suitable for inter stellar travel.

Of course it also depends at what level of interstellar development a species is, if they are just starting out, they may look somewhat like humans, as the human form is also quite common, with between 5 and 15% of nearby extra-terrestrial species resembling humans. If however they have been going interstellar for significant periods of time, then they tend towards the lean bodies and large heads characteristic of Grey aliens, since they tend to shed the more animalistic qualities like sex drive which are pointless in space, and could lead to overpopulation. Long time fourth density beings tend to need specific atmospheric conditions in order to operate for long periods of time, but for short durations they can handle even hard vacuum with minimal ill effects, allowing them to bypass the need to dawn a space suit for short EVA’s.

At the fifth density are the shape shifters, since their bodies are primarily made of light though they may still require some minimal sustenance. The fifth density aliens generally look like the species they interact with, so when interacting with them they may seem like beautiful humans, though there true forms are usually that of a ball of light.

At the sixth density there isn’t really much of a body anymore, so generally they interact with the lower densities either telepathically or by incarnating amongst them. The fifth and sixth density are capable of inhabiting whirl winds and electrical storms, so can have some interesting learning opportunities in the clouds of Venus, as well as those of the gas giants like Jupiter, and ice giants like Neptune. Even living in the plasma layer of a star is viable for them.

Some SciFi’s pretend like Earth would stand a chance battling it out with aliens, but that is completely absurd. You can’t hurt someone that doesn’t have a body, and the galactic guardians have all the tech of thousands of civilized worlds and hundreds of millions of years of development. They have planet busters that can turn the Earth into a new asteroid belt if we try to ‘dig in’ and hide underground. Fighting can achieve nothing good, just like you don’t get into a fist fight with a police officer and expect to come out on top. But even that analogy is too equal, the more accurate would be one that would equate Earth and it’s civilization with a fruit tree. A fruit tree may develop some chemical weapons that inhibits other plants from growing in it’s vicinity, but if the tree is diseased beyond repair, the gardener can come with an chain saw, cut it down and burn it. No the only viable solution is co-operation, strive to help the Earth be a healthy fruit tree. The Galactic Guardians are a benevolent lot.

In summary, if you want the Earth to havea continued place in the galaxy, then live a happy, peacefull life with a sustainable ecological footprint and full of learning experiences. By having such a life yourself and helping others have a a fullfilling life you are helping secure the good favour of the galactic guardians. Remember that some of the people you may be helping have a good life may be galactic guardians themselves.

Organization Finances
=====================

Green Buddhism Finances
-----------------------

The idea in Green Buddhism is to have a sustainable financial situation, that will allow the organization to function in perpetuity.

In order to do this all fixed costs, must be paid by fixed assets, such as trust funds. This allows for people to attend the centre without requiring daily contributions to keep the organization running.

This also means that don’t have to compromise our ethics or values in order to make ends meet. In the long term it also maximizes the value of all the donataions which are received. For example at 3% interest rate, in a mere 34 years each donation will have paid more in interest than its initial value.

Only variable and unexpected costs would be paid from some kind of cash balance, and whatever can be done would be done to avoid lowering the amount available in the trust funds as that can damage the long term health of the organization.

If the trust fund accrues money at an interest rate of 3%, which is a fairly reasonable interest rate. And we take the example of the Owen Sound locality for prices, where space rental for an evening is $30, renting for a full day is $100 and rent for a month is $625, and house rental at $2000 then we can surmise the following:

A trust fund of a certain value can support certain activities:

|               |                 |                                                 |
|--------------:|----------------:|:------------------------------------------------|
|         Amount|  annual interest| useable for                                     |
|      $1,000.00|           $30.00| An annual meditation group meeting              |
|      $4,000.00|          $120.00| A meditation group meetings in each season      |
|     $12,000.00|          $360.00| Monthly meditation group meetings               |
|     $24,000.00|          $720.00| Bi-Monthly meditation group meetings            |
|     $48,000.00|        $1,440.00| Weekly meditation group meetings                |
|     $88,000.00|        $2,640.00| Weekly Meditation, and 12 retreat days annually |
|    $128,000.00|        $3,840.00| Weekly Meditation, and monthly weekend retreat  |
|    $250,000.00|        $7,500.00| Full time meditation centre in an office        |
|    $600,000.00|       $18,000.00| House size meditation centre                    |
|    $333,333.33|       $10,000.00| Full time Bodhisattva (Hydrogen)                |
|     $1 million|       $30,000.00| Helium-3 Sangha (3 Bodhisattvas)                |
|     $4 million|      $120,000.00| Carbon Sangha (12 Bodhisattvas)                 |
|   $5.4 million|      $160,000.00| Oxygen Sangha (16 Bodhisattvas)                 |
|     $8 million|      $240,000.00| Magnesium Sangha (24 Bodhisattvas)              |
|   $9.4 million|      $280,000.00| Silicon Sangha (28 Bodhisattvas)                |
|  $18.7 million|         $560,000| Iron Sangha (56 Bodhisattvas)                   |
|    $21 million|         $630,000| Copper Sangha (63 Bodhisattvas)                 |
|  $35.7 million|       $1,070,000| Silver Sangha (107 Bodhisattvas)                |
|    $66 million|       $1,970,000| Gold Sangha (197 Bodhisattvas)                  |

The general idea is that a Bodhisattva is paid the global average household income. If they are not asexual then they should also be happily married, to avoid any issues with misinterpreted advances.

In developed countries 10 thousand isn’t considered a lot, and if considering they are paid as much as an average person per hour, which in Canada is aroud $25/hr, then they only have to work 8 hours a week. They could have an additional job or business on the side if they feel so inclined.

The Bodhisattva Salary is a basic income. The aim is to make them not have to worry about meeting their basic needs, making them more peaceful, harder to corrupt and generally more beneficial for the community.

Sangha’s larger than the Iron Sangha aren’t really recommended outside some kind of exceptional circumstances. In the universe Hydrogen, Oxygen, Carbon, Silicon and Magnesium and Iron are the most abundant molecule forming elements, so those should likely also be the most common ones as community forming elements.

Temple
======

I’ve been seeing the temple in my visions for a while now, and I didn’t really understand why it has multiple floors, and a crown seat. Now it’s kinda making sense. Inside all the floors and walls are glass. Yesterday I had a vision about how they work.

So the effect is that when you’re on the first floor, you see the people in robes sitting above you, and the robes and hat sitting above that. There is also a glass stairwell, basically anyone that graduates to the 5th level (which is compassion meditation), can then wear the robes and go to the second floor to meditate. There are also lots of windows in it, there is a drawing of the temple in the plan I linked to.

In the temple, everyone sits down to meditate at the same time. And the central area is hollow everyone can see the crown seat (kinda like a suspended glass bowl). The idea is that the acoustics should carry so whomever is guiding the meditation should be heard by all with minimum amplification.

Like if you can imagine a hundred people meditating together in a stacked configuration, it seems to be quite powerful idea to me. Especially if you consider the bodhisattvas and the people of the second floor will be pulling up earth energies that will rush through the people of the first floor, the visitors that have come to check it out. It should be pretty epic.

Sino-Tibetan Politics
=====================

Dorje Shugden is a Buddhist diety that seeks to harm any that mix Gelugpa teachings with those of others — similar to how under Sharia Law it is honourable to kill anyone that stops being a muslim. Dorje Shugden is a rather late addition to the Buddhist pantheon and so may have been inspired by the secterianism of Islam. Some murders and many misfortunes have been ascribed to Dorje Shugden’s sectarian wrath though it is an admitedly complicated issue.

Much of the violence, protests and strife for independent Tibet has been caused by Shugden supporters, some of which have been proven to be funded by a foreign power.

In a way foreign powers funding violently secterian Shugden to terrorize Independent Tibet is repeating recent history. Where the violent extremist Wahabi Islamic groups were funded by foreign powers to terrorize Communists. Hopefully the unnamed foreign powers will see the bad karma that comes from funding terrorists, and instead funnel their money to something good.

Verses of Violence
==================

This is a listing of violent verses which you can redact from your personal copies of these religious texts.

Torah/Old Testament
-------------------

Christianity/New Testament
--------------------------

Some people see Jesus as a loving and compassionate man, and indeed he was, however like all of us he had his moments where he faltered in his compassion.

Luke 10:8–12 and Matthew 11:20–24 describe such a moment, he got angry and you could say one of my old heartless friends took advantage, and had him say those words.

I agree that there is no reason to harbour ill will against anyone, to hold a grudge it hurt oneself, in that sense “forgiveness” makes sense. However I think it is important to remove hate speech and promotion of violence and genocide from what people are allowed to preach or spread, no matter their belief system. In Canada hate speech is a crime, and I don’t think that Christianity should be immune to the law.

I think that just “remembering” is too weak, since Christianity never apologized, and never changed the beliefs that lead to the genocide in the first place, so Christianity is exactly as dangerous as it ever was, even if it is currently “dormant” because it feels it’s force is so overwhelming. However we are already starting to see the rise of “white supremacists” mostly Christian extremists that want to be rid of all ethnic non-Christian people. Of course it’s the same for other genocidal and violent religions like Islam. That’s not to say anything of Christian or Muslim people, who are all different and many of whom don’t even read or follow their religious texts. I’m merely talking about the content of the texts and what they promote, as well as the suffering they have already caused.

I hope that if people are educated about what hate propoganda looks like, they would be able to see it for what it is and filter it appropriately. But I think it could also be worthwhile to have “hate free” (like trans-fat free) editions of various religious texts, which have had the verses of violence removed. As indeed only the safe versions are what anyone can preach or promote in public legally.

I would feel much safer if I knew that the religious texts people in my community were reading were of the safe variety. Because it is exactly when people fall upon hard times that they turn to their religion, and their religious texts, and in the case of Islam they tend become jihadi (holy-war wagers/terrorists), and in the case of Christianity they tend to become supremacists and-or evangelists. We want the texts people turn to in hard times to help people to reintegrate with their community, and help them solve their emotional problems, not cause more problems for the community.

Islam
-----

### Quran

### Hadith

Hinduism
--------

Buddhism
--------
