
\section{Learning of the teaching} % a layman hears a Buddha teach the Dharma. 

About a thousand years ago (~970CE), I was at a market on the coast 
of the Caspian Sea, a merchant told me that up in the mountains peasants had            
learned to become gods, and I wanted it.
We were in the middle of a boasting battle where I had been bragging about
the ability of my gods, and my own skill with the sword. 
The sun was hot, the sand was blowing in the salty air, 
the liquor was flowing. We had had a good journey down the Volga and were 
finishing our trade before departing home.

He probably didn't think much of his boast, and only meant it to undermine my
own tales of the greatness of the Nordic gods. What he didn't know was that I
had been thinking along the same lines. That gods were at sometime men, and had
later done or learned something which made them gods in the first place. In my
soul-searching I was also inclined and interested in becoming a god myself,
though none of my contemporary varingjar seemed too much care for such fancies. 


My dear friend, Carl lets say, would put his hand on my shoulder and tell me,
that when I let go of these aspirations and truly live by the gods without the
vanity and striving to become one of them, then my life will be more peaceful,
and I could retire to tending a herd or farm and having a family. Carl my
brother in arms, was always there for me, but he had his own dreams and destiny. 

With the merchant that evening we had done some more yelling, laughing, toasting
and mead drinking. Though after the commotion had died down, and I had some time
to rest alone with my thoughts. A creeping feeling just began to grow on me,
things started to slide into place, and events in my life seemed to have all
been culminating at this moment. This was a pivotal moment.

\myasterism{}

Though I did not know it at the time, that has been my first exposure to the
teachings of the Buddhism. Some events stay with us for a lifetime, like how
people may recollect when they found out about a massive tragedy, such as the
world trade center collapse on 2001--09--11. Similarly, people may remember more
personal things such as their first kiss, or their first date. Some things
however, are so pivotal that they stay with you for many lifetimes, like bright
beacons in the night, steering you safely. 

This book follows the format of one of the pre-sectarian Buddhist paths, a
twelve fold path from CulaHatthipadopama-sutta, the 
``Lesser Discourse on the Simile of the Elephant's Footprints.'' 

In addition to including lots of real world examples that can at least hope to
capture the attention of the reader, I'm including some true stories from my own
past and present-life that are related to my own journey along the path. 


\section{Resisting the teaching} % questions the teachings and their effect on life

There are many ways in which people resist learning about the aspects 
of awakening. First I'll address some common reasons people use to not engage 
in Buddhism, and then I'll explain some basic terms which often
lead to much debate when they are not made clear. 

\subsection{But I already have a religion}
Some people accustomed to the sectarianism of the Abrahamic faiths (Judaism,
Christianity, Islam) are under the impression that a person can only have one 
faith of a particular sect. For example the various sects of Islam often have
bloody conflicts both internally and externally. Protestants and Catholics in
Ireland also had many conflicts. 

Sometimes North Americans like to ``purify'' various eastern religions, 
by untangling them from their cultures and other admixtures of faith.  
While there is nothing inherently wrong in doing so, the important thing is to 
acknowledge that admixtures are valid and respectable, and not to fall into the
trap of hubris regarding their ``purer'' form. 

Mahayana and Vajrayana Buddhism are non-sectarian, meaning they allow the free
admixing of other faiths. This is why Mahayana areas like in Japan there is 
Shinto Buddhism, and in China Taoism, Chinese Traditional Religions and 
Buddhism can be believed by a person simultaneously. Similarly, in the Tibetan
Buddhism (Vajrayana) all the various forms are considered schools, so a student
may learn from multiple schools, and a person may hold multiple lineages. 

Notably Theraveda and Shugden varieties of Buddhism are sectarian and may look
down upon or strictly forbid mixing with other faiths. 

The non-sectarian aspects of Mahayana and Vajrayana may have multiple factors,
but notably it can be found in the both the Sotapanna 
 vows and Bodhisattva precepts written by Asanga around
300CE called Bodhisattvabhumi. In it thw vow equates ``causing Schism in the
Sangha community'' with killing ones parents, and drawing blood from a Buddha.
I'll go into more depth on the Bodhisattva precepts in Chapter 11
(\ref{chapter11}). But for now it's suffice to say that it is a vow that all
Buddhists that have reached the first stage of enlightenment (Sotapanna) adhere 
to, as does Green Buddhism. Though when selecting a school to join, it is best 
to ask if they respect the Sotapanna vows or the Bodhisattvabhumi, as perhaps
they do not, can also check the appendix regarding some complexities of 
Sino-Tibetan Politics (\ref{sinotibetanPolitics}).

The point of this is that no matter which other faith you adhere to, you can
also adhere to any form of Buddhism that supports the Sotapanna vows or 
Bodhisattvabhumi without abandoning your other faith --- which is a majority of
them. You can be a Christian Buddhist, a Muslim Buddhist, or even a 
Materialist Buddhist.  

\subsection{But I'm an atheist}

Just like with other religions one can be an Atheist Buddhist, as Buddhism does
not require you to worship any gods. The Gautama Buddha that brought the
teachings of Buddhism to Earth was a man who is now dead and has reached
nirvana, so can not be worshiped. His teaching and those of his followers can 
only be learned from. 

When observing various paintings and learning that there are Yiddam or
meditation dieties this can lead to confusion on this point. However, the major
difference is that Yidam's are generally understood to be mental constructs, and
the meditation involves ascribing the attributes of the deity to oneself, during
divine meditation. 

In that sense it is similar to reading about a successful person and how they
lead their life, then adopting their good habits to improve your own. 

\subsection{But I'm not religious}

Some people like to claim that they are not religious, however everyone still
has a belief system. For example a popular one amongst contemporary sceptics is
monist materialism. This is the point where it is important to explain some
simple terms so that we could communicate effectively. 


\subsection{Truth}\label{truth}
In Western Philosophy the meaning of truth, knowledge and existence has been a
raging debate for thousands of years, it has its own field of inquiry called 
Epistemology. 

However, for the purposes of this text, we will take a more linguistic approach,
with a definition based on usage of the word. Of course, I don't mean how
philosophers use the word in epistemological debates, but rather the more common
usage found in day to day interactions. 

When someone is asked to tell the truth, the whole truth, and nothing but the
truth in court. They are asked to convey what they personally believe and-or
experienced. 

When someone tells a true story, it is based on what they personally believe or
experienced. Even if they are telling it in a second or third hand account, at
some point someone believed they experienced it firsthand.

Experience is based on the senses. In Buddhism there are six senses: touch,
smell, sight, sound, taste and thought. More generically an experience can be
considered anything that is an input to a system, that gets processed in some
way to change the internal state of the system. So for example a beam of light
hits your eye, gets processed by your brain to discern meaningful text, and you 
modify your beliefs to integrate the new information. 

A slightly more logic oriented approach to truth is where if a particular 
variable is a certain value right now, then the answer to whether it is that
value is true, and whether it is a different value is false. The example being,
is it true that the sun is shining right now? You would look outside and decide 
whether it true that it was shining or not.

Though opinions may differ from one person to another what they consider to be
shining, for instance some may say that if it is the day time then it is
shining. Others may say that if it is cloudy then it is not shining. While still
others may go a slightly more eccentric route and say that the star we call the
sun is shining now plus or minus four billion years. 

Humans often have to work with incomplete information, so when they experience
something, some part of the experience has to be filled in where there are gaps
in the information. Similarly, every time a human remembers something, they are
actually recreating the memory, and so it always changes slightly from one
telling to another. 

It is important to understand from all this that truth is subjective, and
personal truth can be false in a different context. For example the past life
stories I remember, while they are true for me, are falsifiable, and may have
various inaccuracies, due to errors during download from the soul world, errors
in remembering it, and contamination from other knowledge. 

We can summarize that:

Truth is based on personal experience.

So when someone claims they have ``the one true religion'', then you can understand
that to mean that is the one religion in which they personally believe. 

Truth alone is not sufficient for a complete understanding. As you may recall
there were also the concepts of existence, knowledge and reality which were 
covered in epistemology. 

\subsection{Existence}

Existence is anything that can be imagined by someone. So for example to someone
that doesn't know what Shambhala is, it does not exist. 

Much like truth, existence can also be subjective. To some people Shambhala is a
mythical city where everyone is enlightened, to others it is a branch of
Buddhism with its headquarters in Nova Scotia, Canada. Of course one can also
discern the two using a compound noun phrase, mythical Shambhala for the first,
and Shambhala International for the second. 

\subsection{Belief}

There are several definitions of belief\cite{BeliefDefinitionofBeliefbyMerriamWebster-2018-10-15}, 
but for the sake of this text we will be using the definition of some thought that
is accepted to be true, more specifically we'll treat belief as a basic atom of
thought such as a complete sentence or independent-clause. For example a belief
could be ``the earth is round'', or ``the sun is a ball of hot gas.'' 

Some people like Albert Einstein are visual thinkers, and so their beliefs may be
expressed more visually. Though again for the purposes of this text we'll be
treating beliefs in the form of text, and the independent clause is the smallest
unit of meaningful belief in text. 

While one could say something like ``an apple'', and it would conjure the image of
an apple in the mind, the phrase by itself doesn't convey anything useful
without a context. Whereas ``An apple grows on a tree'', is something that does
convey a bit of knowledge. 

\subsection{Knowledge}

Knowledge is another one of those big words that has a lot of debate around it. 
Speaking plainly knowledge generally refers to what people have experienced
before. When you know a person, then you have met
them before, or interacted with them. If you
remember personal details they shared about themselves, then could say you know 
them personally. 

Now we get to a distinction between personal knowledge, and shared knowledge.
For example facts are a part of shared knowledge, which brings us to the topic
of reality. 

\subsection{Reality}

Real is the group of beliefs held in common amongst a group of people. 
Objective reality includes objects, such as rocks, plants, animals, planets as
people with beliefs. 

So while a group of homo-sapiens may share the belief that the Earth is flat, 
this does not change the beliefs the planet and galaxy cosmos it is nested
within holds. 

Importantly facts are elements of the real. To claim something to be real or a 
fact it must be verifiable. 

For example if you claim to have gone swimming in a frozen lake, and your
friends asks ``is that a fact?'', then you can show them the video you took, for
an objective reality verification, or your other friend that was there could
corroborate. 

There can be things that may be objectively real, but difficult to verify, 
such as past lives on different planets. Generally for difficult to verify
claims, it is better for the claimer to lower their claim status from real to a
personal truth. 

\subsection{Animism}
Some people like to divide the world into people and not people or into 
thoughts and things. Most tribal belief systems, the beliefs of
children, see all or most everything and everyone as having soul.

\subsection{Dualism}

As the Dalai Lama mentions in his book on Science and
Buddhism\cite{9780316726436}, materialist scientists often have a dualistic 
world view, where macroscopic things are based on Newtonian Mechanics, but at
the quantum level they agree that the observer and the observer are
intrinsically interrelated. 

In particular the separation of mind and matter is called dualism, as it
separates creation into two. However, there are a variety of problems with this
view, including that there is no way that mind and matter could interact if they
were separate, it violates the laws of physics, doesn't make sense in biology,
and is an overly complicated way of looking at the world. 

While dualism can be an interesting thought experiment, 
for mind and body to interact one is derived from the other, 
which is considered monism. 


\subsection{Monism}\label{monism}

Some people believe there is only matter (materialists), and some people believe
there is only mind (idealists). Considering a variety of factors such as 
information theory, near death experiences, out-of-body experiences, past lives,
and a whole host of other non-physical experiences that people regularly have, 
indicates that it is mind that is the whole, and thus idealism is correct.
There are a few varieties of idealism, like Advaita Vedanta of Hinduism, and
Yogachara of Buddhism, Neoplatonism of Ancient Greece, though the main one 
popular in science and compatible with quantum and classical physics is 
pluralistic idealism.  Again there are several varieties of pluralistic 
idealism, in this book is a form that seems to work. 

The physical world we experience with our senses, is us observing the
communication of a large variety of belief holders (atoms inclusive), mostly 
through what we in physics term bosons, which include photons also known as 
light particles. 

Even with ``solid'' things like an atom, which is considered fermionic and thus
different from bosons, they don't actually `touch' each other, even during
nuclear fusion. Instead, they communicate to each other with bosons giving each 
other space, much as how it is difficult to push two magnets together who have 
their south poles facing each other. Another example is gluons which are a type
of bosons that is the communication used to keep protons and neutrons together. 

Incarnation is like a car rental, we are souls in the soul world, and we want to
play this fun game, called Galaxy Cosmos, the Greeks called this game Sophia. 
So we are within the Sophia galaxy cosmos, and are incarnated on Gaia,
borrowing some of her atoms to maintain the bodies our parents nurtured into 
the world.

According to Integrated information theory, every proton has a semblance of
consciousness because it is able to preserve state and communicate. For example
an atom can have an energy level, and it can communicate with photons. When you
send it a photon, it reflects it back, saying ``I am here, this is my color.''
Though that is a gross oversimplification as it has much more complicated
communication depending on a large variety of factors. 

Much of our accrued knowledge has been gathered by science. 

\subsection{Science}

Science, and particularly the natural sciences are focused on discovering what
mineral and DNA based life forms believe. 

However, science is more generic than that, and is actually more closely aligned
to a specific method of inquiry known as the ``Scientific Method.'' 

In brief in the scientific method a person comes up with a hypothesis. These
usually come in two forms, the Null Hypothesis, or the If-Then hypothesis. 

The Null hypothesis states that two things are unrelated. For example ``What a
person remembers as their past lives does not influence their present life.''

An If-Then hypothesis states that there is a causal connection between two
things. For example ``If a person is hypnotically regressed to a time before they
were born then they will experience either their past lives or the soul world.''

Then one comes up with an experiment to test the hypothesis. Such as
hypnotically regressing a bunch of people to a time before they were born.
Though ideally would also have some kind of control group, such as a group of
people that were asked to make up a story about someone from the same time
period. 

When someone says ``scientists agree'', or ``science has shown us that'', typically
they are referring to some studies that have been accepted as valid by at least
some group of people. 

Notably there is a fairly strong group of materialists amongst
scientists, in a way they have a world of their own. 

\subsection{World}
A world can be considered to be a particular group of people that 
share certain beliefs, and that keep those beliefs alive orbiting each other, 
much like a planet. 

So for instance Buddhism is a world of belief that was started on this planet by
Gautama, though he himself carried it over from another planet that he had lived
on. And even after he left the Buddhist world lives on. 

These worlds are self-reinforcing and often it's members can blindly disregard
evidence to the contrary of their world beliefs, in order to support the group.
This can be seen amongst religious groups, political groups,

At this time one of the world groups that hold the most influence at this time 
is the sceptics, particularly of the european flavour of materialism.

\subsection{Sceptics}

Sceptics and disbelievers in general all serve a purpose, 
they add diversity to creation. 

The Materialist faith, and that of the one-lifers is one that has been around
for thousands of years. It is useful for some kinds of lessons to focus on just
this one life, and the champion sceptics that promote the one-life belief
systems are helping to create an environment where it is easier to forget about
the soul world and past lives. 

A world where it's easier to focus on the present,
particularly for those that have trouble in this regard. 

The important thing is to cultivate respect, love and understanding. Recognize
that the sceptics and disbelievers are providing a valuable service, by allowing
people to have choices about which world they wish to live in. 

Similarly, by giving others the freedom to have their worlds of beliefs, we
deserve to have the freedom of our own world of belief. 

The materialist faith is not the only one, and there are many science
experiments that disprove it's tenents. Though just as with any world, the
believers disregard evidence to the contrary. 

Green Buddhism however is not materialist, instead it is monist but with the
flavour that all is mind or information.  Let's have a look at the physics of
information. 

\subsection{Entropy}
In the second law of thermodynamics it states that entropy is always increasing. 
Entropy is the name for the energy that is lost  or the ``waste heat'' that 
is created in most physical process. 

Information theory uses the word similarly, Shannon was famously looking
for a word to describe the ``missing information'' lost during communication, when
von Neumann suggested calling it entropy as it was the most similar
concept.[\cite{9812384006}]

If we look deep into physics we find that matter is made up of atoms, which in
turn are small entities that communicate to each other through little
information packets called bosons. A photon or light particle is an example of a
boson. 

So when you are holding a rock, what is really happening, is that the atoms in
your hand, are communicating to the atoms in the rock, and informing each other
about their location, and also saying ``okay, that's close enough'', it takes a
lot of pressure to make atoms move closer together, since just like a majority
of people, they don't like it when they are being squeezed by a crowd.  
Different atoms though have different preferences, some like flourine always 
want to attach to someone, and metals are fairly good at sticking together. 

Now someone might complain that I'm anthropomorphizing here, saying that
flourine wants something. However, as we'll cover later, wanting or desire is not
born of choice like a wish but rather a preprogrammed response, as are animal 
and plant instincts. In that regard it is appropriate to say an atom wants.

In fact, with Integrated Information Theory (IIT) we know that atoms not only want,
but they also have a semblance of consciousness. IIT says that even a
single proton, which is the simplest form of atom, has consciousness. To analyze
whether something has consciousness there are a variety of factors involved,
however to simplify it is any system that has inputs, outputs and state. 

So for instance the atom has a state of its current energy, it can receive a
photon from the sun, absorb some of its information, and produce a different
photon which has a colour representing itself. 

In this way we know that our bodies are made entirely of conscious beings, the
atoms, the cells, perhaps some organs, and then the body which you've
incarnated into, providing the wisdom and intelligence of your many
lifetimes, even if you aren't consciously aware of them. 

Going back to entropy we know that the ``waste heat'' is made of information
packets. 

Information Theory reformulated the second law of thermodynamics as saying that
information can not be destroyed. So given an example of a simple classical
computing logic gate, where you have two input bits, and one output bit, there
is some lost information. That lost information is not lost to creation, instead
it escapes as a heat based information packet. 

It is also possible to create a reversible logic gate where there is no loss,
such as a controlled flip (Toffoli) or a controlled swap (Fredkin) gate, they 
have three inputs and three outputs,
allowing you to reconstruct the input from the output, and these kinds of gates
could theoretically achieve zero heat generation, and certainly no information
loss, which goes to show that entropy is really lost information. 

What lost information is brings us to the discussion of soul. 

\subsection{Soul}\label{soul}

Buddhism and theologians in particular can be rather finicky with words. A
Theologian once scolded me for using the words soul and reincarnation in a
Buddhist context because Buddhism has its own synonyms for those, because there 
are some fine philosophical distinctions. Though for this text the distinction 
between reincarnation and rebirth, as well as 
mindstream and soul are without merit, and would only serve to confuse readers.
Also in case this makes your academic mind very concerned I will cover the
Buddhist points of distinction in Chapter 7\ref{chapter7}. 

To simplify the matter for those that are unaware, the main objection early
Buddhists had to the idea of soul, was that it was considered independent and
unchangeable, which contradicts the facts that things are continuously changing, 
and `emptiness' which is better understood as `all is one'.

Interestingly Buddhism kept reincarnation, karma and all the other things, and
instead renamed soul as mindstream, which is basically a changeable version of
soul, which is not as independent. 

Since the majority of the world still uses the word soul for the concept, 
we'll be using the world soul in this text, with the understanding that it is
actually a collection of knowledge, and that as we learn, that knowledge 
transforms. 

This is directly derivable from the second law of thermodynamics, that
information can not be destroyed. This is known in quantum information theory 
as the no-delete theorem\cite{QuantumnodeletingtheoremWikipedia-2018-10-01}. 

So at the dissolution of a host-body such as
the vehicle you are using now, all the knowledge and perspective you have
acquired must continue. The knowledge also can't break down into their base
elements as that would be simplifying things and thus losing information. 

What information theory tells us is that the information can only be moved
somewhere else, in the case of the information that is in our minds at the end
of life, the location it moves to is typically the soul world. 

Interestingly we do have quite a bit of empirical research available on the soul
world, what it looks like, and how it functions, largely thanks to the Hypnotic
Regressions conducted by the Newton Institute, though there is also 
corroboration from the work of other hypnotic regression researchers such as
Dolores Cannon.

To limit the length of this chapter we'll cover the soul world in more depth in 
Chapter 9 (\ref{chapter9}).

Of course to some materialists I've just said some things which may have made 
them flustered. In particular using hypnotic regression as method of inquiry to
delve into the nature of reality. However, as we've already learned that the
results of those hypnotic regressions are simply the truth of those people. 

Humans are often used as the subjects of scientific experiments, and before the
advent of video cameras and audio recorders they were also responsible for
seeing and hearing the results of the experiments. Currently we do not have a
technological means of delving into the soul world, but we do have humans almost
all of whom have the ability during hypnosis. 

Some Buddhists may also be confused how this relates to Buddhism, so we need at
least one more subsection in this introduction, on how hypnosis and guided 
meditation are synonymous. 

\subsection{Guided Meditation or Hypnosis}
For many beginners the easiest introduction to meditation is known as guided
meditation. It is where a teacher or recording of a teacher guides you through
the steps of relaxing your body and suggests ways of performing the meditation. 

What is less talked about is that this is exactly the same process by which 
hypnosis occurs.  Where the hypnotist guides you through relaxing and then gives
some suggestions of what to think about. 

While it's possible to call meditation self-hypnosis, and to call hypnosis
guided-meditation, why use two words when one explains it? Probably just for
marketing purposes. 

However, when you realize that guided meditation is hypnosis, then you can
combine all the knowledge we have from both labels and have greater awareness of
what is going on and how to accomplish it.

The main distinction is that meditation is an active self-guided processes,
wheras hypnosis is a passive external-guided process. So for example under
hypnosis you won't need to use the executive function parts of your brain nearly
as much as in ordinary meditation. 

The important thing for now is to understand that while first person meditation
can be used for personal introspection, every tool has a degree of error in its
perception. Humans are known to have a fairly high degree of error in terms of
witness testimony, as we mentioned earlier, because of a tendency of filling
gaps in knowledge automatically. 

Hypnosis allows us to use humans as tools for viewing the soul world in a 
standardized fashion. This way can regress tens or hundreds of humans to the
time between their past lives, and thus take out much of the filler, and just
get at the content of the soul world. 

In fact this has already been done and accomplished by David Newton and the
Newton Institute. Can read his books ``Journey of Souls'' and ``Destiny of Souls''
for his personal research, and the Newton Institute also has ongoing periodical
of updates based on their latest findings. 

I'll go into more depth on hypnotic regression, reincarnation and accessing 
past lives and the soul world in chapter nine (\ref{chapter9}).

\subsection{Remaining Resistance}

If there are still some resistance you are facing regarding learning more
about  Buddhist teachings, and those of Green Buddhism in particular. Then
please contact us and explain the issues you are facing, so that we can include
or modify the content of future editions to be more compatible. 

\section{Committing to learn the teaching} % comes to have faith in the teachings. 

Perhaps you've marvelled at how the Dalai Lama can be so jolly, or how so many
Buddhist teachers seem to be so calm and to have it all together. Or perhaps you
want to be more like those highly productive and quite rich people that
integrate meditation into their daily routine. 

In this book you'll learn all those things. The main thing is that we have to 
learn the teachings, test and apply the practice that work for your situation, 
and persist until we persevere. Of course along your journey to becoming a
highly productive Bodhisattva you will be reading many books, doing many
meditations, learning from many teachers, testing many teachings. 

A critical part of my own journey into Buddhism was letting go of the past, and
learning to take advantage of the opportunities I was offered in the present.
Staying on the path, takes one step at a time. 

%  how does it improve their status

\myasterism{}

Later the evening after that fateful conversation on the shores of the Caspian 
Sea, I went to the boat to say goodbye.  

I told Carl that I would be staying on this side of the sea, or rather that I 
had hired a guide and supplies with my share of the sales, which would take me 
along the silk road to the land where men become gods. 

Carl was shocked at first, ``But how can you do that? We need you, you are part of
our crew!''

``You're a skilled seaman and your sword knows no barrier, you'll be fine without
me.'' I said, tugging at his jacket and giving him a firm pat on the shoulder. He
really had nothing to complain about, we'd lost men in much more dire situations
than this. 

``But, we'll miss you.'' he went on, somewhat conceding to the reality of what I
had done. I was already wearing a turban and other desert clothing in
preparation 

``You may go home to your lovely wife and share the spoils of our journey with
her and the children. You may die old with a home full of laughter and stores
full of grain. But I may go and become a god, then I will greet you in heaven,
and we'll decide who took the wiser route.''

``May we meet at the gates of Thor.'' We gave a slight bow of acknowledgement,
reminding me of his agricultural god. 

``And drink in the halls of Odin!'' I replied as the god of death was my master. 

We hugged farewell, and I was off onto my journey. 

\section{following and supporting the teaching} 
% and makes a habit of following and supporting the dharma 

Affa as I called him was my guide, he had a longer name, but I couldn't really
be bothered to learn it, seeing as I was paying him, and he was only a stepping
stone on my journey of godhood. His norse was rather the worse for wear, and we
didn't really end up talking much. 

The journey started out tamely enough, with each of us on a camel loaded with
supplies.  Though I think he had the thought that I would get bored with this
journey and decide to turn back at some point, at which point he would get to
keep much of the coin for much less work. 

There was a point where Affa tried to kill me and take my money. I was having
some trouble breathing and woke up to see Affa barring his teeth and trying to
strangle me with one hand and had a dagger in the other. The man was an idiot, 
he was less than half my size and woke me up before going for the stab. We
wrestled for a bit, and he ended up losing some fingers to his dagger. 

Couldn't kill him as we were alone in the middle of a foreign desert, and
without him I would surely have died of exposure. I couldn't exactly let
him think that attempts on my life would go unpunished. The missing fingers 
occupied him for the rest of his time with me till he passed me off to a sherpa.

I think that if I had been kinder to Affa, learned his real name
and communicated with more than a few grunts and choice words, he probably
wouldn't have tried to kill me. 

As we progressed in our journey through the desert and got to the relatively
rocky areas. At this transitional zone Affa met with a Sherpa, the one that had
told him about the peasants becoming gods in the mountains. 

He seemed sceptical, though I didn't quite understand what they were saying it
went something like. ``Are you serious?'' asked the Sherpa somewhat aghast.

At which point Affa raised his maimed and bandage hand and with tears in his
eyes said ``he's deadly serious.''

The sherpa did not give me much trouble, nor attempt to take my life. 

In those rocky mountain areas we just had a pack mule and were otherwise on foot. 

Throughout it all I had been carrying my sword, which I had to wrap in cloth and
carry on my back, so as not to ``cause trouble'', attract thieves, or start an
incident.  As we got higher into the mountains, the people got smaller, the
going got harder, everything was colder, and I was a lot hungrier. 

As we were approaching Guge (ancient western Tibet), my guide started to get
ancy, but I told him unless he could find me another guide he'd have to take me
all the way to my destination. Used some threats, but I think mostly he kept on
with me as he did not want to give the burden of me onto anyone else. 

About the prowess of his own people that had led to me coming along this
journey. So in a way he accepted full responsibility for what he had said.
An exemplary man, whom I hold in the highest regard. 

For a long time we walked on the Tibetan plateau. At times, we would stop to
resupply and I would ask if we were close. He would nod and say that we will get
there. 

At various huts along the way, he had a way of convincing them to let
us stay. I always slept with my sword in hand at my bedside. 
At night, he would sleep sitting up outside my door, and warn away
people lest they wake me. ``he will as soon rise as slice you in half'' the
sherpa would mutter. 

At times, we stopped by the houses of holy people. One memorable one was a man
with a black flowing outfit with gold trim. Presumably a man of some
importance. The Sherpa and he talked for a while at the dinner table after I
retired to bed.

One time we were making our way across the plateau. It was a hazy day, and I was
using my sword as a walking stick by this point, when a boy ran up to us. Though
he was small as many of the other locals, I had a newfound respect for these
small people, as they could run in the thin air, and they did not seem to tire
as easily. Wheras I always felt weak and tired, struggling with the rarefied
air. 

The Sherpa and the boy talked for a little while and then the boy ran off. I
asked what it was about, he gave me a weary glance and said ``we're close,
they'll be expecting us.'' I was too tired to pry into the deeper meaning behind
that. 

Eventually frostnipped and hagard we reached a fortified town of Guge. The fog
made it, so we could not see it until we were within a few hundred meters.
The walls had a red border, and archers lined them. There was a small troop with
shields and sticks guarding the entrance. They were expecting us. 

A wagon was approaching the town, with
a few women onboard. A man in an ornate silk jacket with gold trim standing
between the troop and the wagon seemed to be the town leader.

I took a moment to take this all in, I looked around and noticed that my Sherpa
had hung back to my right, and was several meters away. He nodded his head
towards the fortress, ``we're here'' he projected his voice. Watching me wearily,
to see what I would do. 

Turning back to the fortress, leaning on my sword, I had mentally rehearsed for
this moment for a while. I would challenge their gods in battle, and after
defeating them, I would be a god myself. At least that was my plan, for if one
kills a god, then is one not a least as mighty? 

With my remaining strength, I threw off my robes, raised my sword with one arm 
into the air and yelled ``Let me see your gods, I will defeat them!.'' The hail
of arrows pierced me in a dozen places shortly therafter, while my arm was still
raised. 

I collapsed forward, and the last thing I saw was a woman in the wagon staring
at me in shock, and the leader walking towards me in his silk and gold, a young
Khor-re perhaps.

\myasterism{}

The Sherpa was a good man, even if he led me into a trap, he did right by his
people. He had gotten out of the way just in time to avoid the hail of arrows. 
My guide into the land of Dharma was a realized man himself. 

I sacrificed my life and all I had in pursuit of the teaching (Dharma), even if 
I was confused about how it worked and what it was. In between lives I was given
the option to go back to the varingjar and be born as one of Carl's children 
or to go forward with learning the Buddhist teaching.

The woman in the wagon, and the leader had a romance, and I chose to incarnate
as the product of that affair. A thousand years later, I still think it was a
good choice.

\section{Dana}

In a way I sacrificed my life to gain access to the teachings of awakening
divinity. In addition, I also sacrificed my sword to the Tibetan plateau. Those
were my initial gifts my first Dana. 

Now I wish to give you the opportunity for Dana, in a safer and more controlled
setting, particularly if you received this book for free. 

To make it affordable for the farmer and the industrialist, can make it ten
percent of your average day's income. Since this book should occupy you for at
least two and a half hours or ten percent of the day. 

For example the median human on Earth in 2018 makes roughly \$5,000 per year, 
divided by 365 is \$13.69 per day, and multiplied by 10\% is \$1.36. 

Can make your donation to the Green Buddhism, with the name of the book
in the comment. 

Practicing charity helps people feel good, and achieve the first Buddhist
perfection of Dana. And of course you can continue reading without donating,
because only a voluntary donation is one that shows true Dana.

All real Buddhist teachers accept donations to give their followers the
opportunity to practice Dana.

Now let us go on a journey to learn about virtue, and how to be a virtuous
Buddhist. Virtue is integral to being successful in life, with health, wealth, 
love and liberty.

